package com.siemens.gs.it.ads.data;

import java.io.Serializable;

public class PresentableUnit implements Serializable {
	private static final long serialVersionUID = 1L;

	private boolean favorite;
	private String aeic;
	private String unitName;
	private String id;
	private String type;
	public boolean isFavorite() {
		return favorite;
	}
	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}
	public String getAeic() {
		return aeic;
	}
	public void setAeic(String aeic) {
		this.aeic = aeic;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFavoriteClass() {
		return isFavorite() ? "filled" : "";
	}
}