package com.siemens.gs.it.ads.service;

import java.util.List;

import com.siemens.gs.it.ads.data.PresentableUnit;

public interface UnitService {

	public List<PresentableUnit> findAllUnits();
	
	public PresentableUnit findUnitById(String id);
}
