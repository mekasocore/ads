package com.siemens.gs.it.ads.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.siemens.gs.it.ads.data.PresentableUnit;

@Service
public class UnitServiceImpl implements UnitService {

	private Map<String, PresentableUnit> allItems;
	
	@PostConstruct
	private void init() {
		List<PresentableUnit> units = new ArrayList<>();
		for (int i = 0; i < 500; i++) {
			units.add(testData(i));
		}
		this.allItems = units.stream().collect(Collectors.toMap(PresentableUnit::getId, Function.identity()));
	}
	
	@Override
	public List<PresentableUnit> findAllUnits() {
		return new ArrayList<>(this.allItems.values());
	}
	
	private PresentableUnit testData(int counter) {
		PresentableUnit unit = new PresentableUnit();
		unit.setFavorite(counter % 5 == 0);
		unit.setAeic("8791234" + counter);
		unit.setId(String.valueOf(counter));
		unit.setUnitName("Acadia 1." + counter);
		unit.setType("CT");
		return unit;
	}

	@Override
	public PresentableUnit findUnitById(String id) {
		return this.allItems.get(id);
	}
}