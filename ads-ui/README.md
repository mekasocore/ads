# ads-ui

This project can be used as a starting point to create your own Vaadin application with Spring Boot.
It contains all the necessary configuration and some placeholder files to get you started.

## Running the application

The project is a standard Maven project. To run it from the command line,
type `mvnw` (Windows), or `./mvnw` (Mac & Linux), then open
http://localhost:8080 in your browser.

-- -Dhttps.proxyHost=194.138.0.25 -Dhttps.proxyPort=9400 -Dhttp.proxyHost=194.138.0.25 -Dhttp.proxyPort=9400 -Dhttp.useProxy=true

## Deploying to Production

To create a production build, call `mvnw clean package -Pproduction` (Windows),
or `./mvnw clean package -Pproduction` (Mac & Linux).
This will build a JAR file with all the dependencies and front-end resources,
ready to be deployed. The file can be found in the `target` folder after the build completes.

Once the JAR file is built, you can run it using
`java -jar target/ads-ui-1.0-SNAPSHOT.jar`

## Deploying to Heroku Cloud
- cd C:\projekte\code\siemens-energy\ads\ads-ui
- heroku login
- heroku plugins:install java

- heroku create --region eu mekaso-ads
- heroku deploy:jar target/ads-ui-0.0.1-SNAPSHOT.jar -a mekaso-ads
- heroku open -a mekaso-ads
- heroku logs --tail -a mekaso-ads

## ToDo
- filter for unit table
- edit for unit table
- configure columns
- unit details
- add unit

