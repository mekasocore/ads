package com.siemens.gs.it.ads.characteristics;

import java.util.stream.Stream;

import com.siemens.gs.it.ads.apptheme.component.SeAccordion;
import com.siemens.gs.it.ads.apptheme.component.SeButton;
import com.siemens.gs.it.ads.apptheme.component.SeIcon;
import com.siemens.gs.it.ads.apptheme.component.SeIcon.SeGalleryIcon;
import com.siemens.gs.it.ads.apptheme.component.SfIcon;
import com.siemens.gs.it.ads.apptheme.component.SfIcon.SiemensIcon;
import com.siemens.gs.it.ads.apptheme.layout.WithStyle;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.accordion.AccordionPanel;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.checkbox.CheckboxGroupVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.datetimepicker.DateTimePicker;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.H6;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.listbox.MultiSelectListBox;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.radiobutton.RadioGroupVariant;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;

public class ShowcaseLayout extends FlexLayout implements WithStyle {
	private static final long serialVersionUID = 1L;
	
	private FlexLayout content;
	
	public ShowcaseLayout() {
		addClassNames(FLEX_WRAP, WIDTH_FULL);
		this.content = new FlexLayout();
		this.content.setFlexWrap(FlexWrap.WRAP);
		this.content.setJustifyContentMode(JustifyContentMode.EVENLY);
		this.content.addClassNames(MARGIN_TOP_M, ITEM_GAP_HORIZONTAL_M, ITEMS_ALIGN_START);

		Tabs tabs = new Tabs();
		tabs.addClassNames(WIDTH_FULL);
		tabs.addThemeVariants(TabsVariant.LUMO_EQUAL_WIDTH_TABS);
		Tab buttons = new Tab(SfIcon.withIcon(SiemensIcon.buildingBlock), new Span("Buttons"));
		Tab typo = new Tab(SfIcon.withIcon(SiemensIcon.legal), new Span("Typo"));
		Tab form = new Tab("Form elements");
		Tab icons = new Tab("Icons");
		tabs.add(buttons, typo, form, icons);
		tabs.addSelectedChangeListener(sce -> {
			content.removeAll();
			if (sce.getSelectedTab().equals(buttons)) {
				showButtonsLayout();
			} else if (sce.getSelectedTab().equals(form)) {
				showFormElementsLayout();
			} else if (sce.getSelectedTab().equals(typo)) {
				showTypoLayout();
			} else if (sce.getSelectedTab().equals(icons)) {
				showIconsLayout();
			}
		});
		add(tabs, this.content);
		tabs.setSelectedTab(buttons);
		showButtonsLayout();
	}
	
	private void showIconsLayout() {
		Stream.of(SeGalleryIcon.values()).forEach(icon -> {
			SeIcon seIcon = new SeIcon(icon);
			Div div = new Div(seIcon, new Span(icon.name()));
			this.content.add(div);
		});
	}

	private void showButtonsLayout() {
		Button primary = new Button("Primary");
		primary.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		Button secondary = new Button("Default Style");
		Button tertiary = new Button("Tertiary");
		tertiary.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
		SeButton seButton = new SeButton("Siemens Energy");
		this.content.add(seButton, primary, secondary, tertiary);
	}
	
	private void showFormElementsLayout() {
		SeAccordion accordion = new SeAccordion();
		TextField tf = new TextField("Full name");
		TextArea textArea = new TextArea();
		textArea.setLabel("Comment");
		textArea.setValue("Great job. Well done!");
		NumberField euroField = new NumberField();
		euroField.setLabel("Balance");
		euroField.setValue(200.0);
		Div euroSuffix = new Div();
		euroSuffix.setText("€");
		euroField.setSuffixComponent(euroSuffix);
		EmailField emf = new EmailField("E-Mail");
		FlexLayout layout = new FlexLayout(tf, euroField, emf, textArea);
		layout.addClassNames(ITEM_GAP_M, FLEX_WRAP, JUSTIFY_EVENLY);
		accordion.add("Fields", layout);
		ComboBox<String> cb = new ComboBox<>("Number");
		cb.setItems("one", "two", "three");
		CheckboxGroup<String> cbg = new CheckboxGroup<>();
		cbg.setLabel("Status");
		cbg.setItems("In Progress", "Ready", "Cancelled", "Waiting");
		cbg.select("Ready", "Waiting");
		cbg.addThemeVariants(CheckboxGroupVariant.LUMO_VERTICAL);
		MultiSelectListBox<String> listBox = new MultiSelectListBox<>();
		listBox.setItems("Go", "Leave", "Stay");
		listBox.select("Go", "Stay");
		layout = new FlexLayout(cb, cbg, listBox);
		layout.addClassName(ITEM_GAP_M);
		AccordionPanel accordionPanel = accordion.add("Boxes", layout);
		accordionPanel.addClassName(CONTENT_STRETCH);
		
		DatePicker dp = new DatePicker("Pick a date");
		DateTimePicker dtp = new DateTimePicker("Pick a time");
		Select<String> select = new Select<>("Five", "Six", "Seven");
		select.setLabel("Single select");
		RadioButtonGroup<String> radioGroup = new RadioButtonGroup<>();
		radioGroup.addThemeVariants(RadioGroupVariant.LUMO_VERTICAL);
		radioGroup.setLabel("Travel class");
		radioGroup.setItems("Economy", "Business", "First Class");
		layout = new FlexLayout(dp, dtp, radioGroup);
		layout.addClassName(ITEM_GAP_M);
		accordion.add("Selection", layout);
		this.content.add(accordion);
	}
	
	private void showTypoLayout() {
		H1 h1 = new H1("H1 very large");
		H2 h2 = new H2("H2 large");
		H3 h3 = new H3("H3 medium");
		H4 h4 = new H4("H4 medium");
		H5 h5 = new H5("H5 small");
		H6 h6 = new H6("H6 very small");
		Span span = new Span("Span text");
		Paragraph par = new Paragraph("Paragraph text");
		Div div = new Div(new Text("Simple text"));
		this.content.add(h1, h2, h3, h4, h5, h6, span, par, div);
		
	}
}
