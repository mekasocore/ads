package com.siemens.gs.it.ads.spring.security;

import java.io.Serializable;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.siemens.gs.it.ads.core.domain.user.AdsUser;
import com.siemens.gs.it.ads.core.service.user.CreateUserUseCase;
import com.siemens.gs.it.ads.core.service.user.ReadUserUseCase;
import com.siemens.gs.it.ads.core.service.user.UpdateUserUseCase;
import com.siemens.gs.it.ads.core.util.DateTimeUtil;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.VaadinSessionScope;


@SpringComponent
@VaadinSessionScope
public class ApplicationSecurity implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger LOG = LoggerFactory.getLogger(ApplicationSecurity.class);

	private static final String DUMMY_GID = "Z002RWXF";

	private final ReadUserUseCase readUserUseCase;

	private final CreateUserUseCase createUserUseCase;

	private final UpdateUserUseCase updateUserUseCase;

	private AdsUser currentUser;

	private final Environment environment;

	public ApplicationSecurity(CreateUserUseCase createUserUseCase, ReadUserUseCase readUserUseCase, UpdateUserUseCase updateUserUseCase, Environment environment) {
		this.readUserUseCase = readUserUseCase;
		this.createUserUseCase = createUserUseCase;
		this.updateUserUseCase = updateUserUseCase;
		this.environment = environment;
	}

	public AdsUser getCurrentUser() {
		if (currentUser == null) {
			LOG.debug("No current user for this session, getting logged in user");
			currentUser = getLoggedInUser();
		}
		return currentUser;
	}

	public AdsUser updateUserProfile(AdsUser user) {
		currentUser = getLoggedInUser();
		if (user.equals(currentUser)) {
			currentUser = updateUserUseCase.updateUserProfile(user);
			return currentUser;
		} else {
			throw new IllegalArgumentException(String.format("Can't exchange current user with different one [%s, %s]", currentUser.getGid(), user.getGid()));
		}
	}

	private AdsUser getLoggedInUser() {
		String gid = getLoggedInGid();
		Optional<AdsUser> user = readUserUseCase.find(gid);
		if (user.isPresent()) {
			return user.get();
		} else {
			// HACK: using some fixed timezone for creation.
			// I think it would be better to read time zone from browser and determine
			// timezone; mabye even use IP to find more specific location/timezone
			// WebBrowser webBrowser = getPage().getCurrent().getWebBrowser();
			// String[] availableIDs2 =
			// TimeZone.getAvailableIDs(webBrowser.getRawTimezoneOffset());
			// unfortunately, UI is not available when user is logged in so we probably have
			// to move this code to ApplicationUi
			return createUserUseCase.create(gid, DateTimeUtil.getDefaultUserTimeZone());
		}
	}

	private String getLoggedInGid() {
		String gid = DUMMY_GID; //SecurityUtils.getCurrentUserGid();
		if (gid != null) {
			LOG.info("Authorized User: {}", gid);
			return gid;
		}

		if (SecurityContextHolder.getContext() != null && SecurityContextHolder.getContext().getAuthentication() != null && "ads".equals(SecurityContextHolder.getContext().getAuthentication().getName()) ) {
			LOG.info("Using dummy user for development: {}", DUMMY_GID);
			return DUMMY_GID;
		}

		throw new AccessDeniedException("No authorized User available");
	}
	
	public Authentication getAutheticationObjet()
	{
	       return SecurityContextHolder.getContext().getAuthentication();
	}

}