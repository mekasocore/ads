package com.siemens.gs.it.ads.symbolicnames;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.siemens.gs.it.ads.AdsApplicationLayout;
import com.siemens.gs.it.ads.AdsContentLayout;
import com.siemens.gs.it.ads.spring.AdsMessageSource;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.router.Route;

@Route(value = "symnames", layout = AdsApplicationLayout.class)
@PreserveOnRefresh
//@AllAuthenticatedUsers
public class SymbNamesView extends AdsContentLayout {
	private static final long serialVersionUID = 1L;
	
	private AdsMessageSource messageSource;

	@Autowired
    public SymbNamesView(AdsMessageSource messageSource) {
    	super();
		this.messageSource = messageSource;
    }
    
    @PostConstruct
    private void init() {
    	setHeaderText(this.messageSource.getMessage("view.symbolicName"));
    }
}