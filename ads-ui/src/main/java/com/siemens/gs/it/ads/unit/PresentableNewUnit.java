package com.siemens.gs.it.ads.unit;

import com.siemens.gs.it.ads.core.domain.unit.UnitType;

public class PresentableNewUnit {
	
	private static final long serialVersionUID = 1L;
	
	private String aeic;
	private Long unitId;
	private String unitNumber;
	private String unitName;
	private UnitType unitType;
	private String dataSourceName;
	private String dataSourceNameSoe;
	private String dataSourceNameFr;
	private String plantName;
	private String plantConfig;
	private String plantDataSourceNames; 
	private String plantDataSourceNamesSoe; 
	private String plantDataSourceNamesFr; 
	private String tagPrefix;
	
	public PresentableNewUnit(String aeic, Long unitId, String unitNumber,String unitName, UnitType unitType, String dataSourceName,String dataSourceNameSoe,String dataSourceNameFr,String plantName ,String plantConfig,String plantDataSourceNames,String plantDataSourceNamesSoe,String plantDataSourceNamesFr,String tagPrefix) {
		super();
		this.aeic = aeic;
		this.unitId = unitId;
		this.unitNumber = unitNumber;
		this.unitName = unitName;
		this.unitType = unitType;
		this.dataSourceName = dataSourceName;
		this.dataSourceNameSoe = dataSourceNameSoe;
		this.dataSourceNameFr = dataSourceNameFr;
		this.plantName = plantName;
		this.plantConfig = plantConfig;
		this.plantDataSourceNames = plantDataSourceNames;
		this.plantDataSourceNamesSoe = plantDataSourceNamesSoe;
		this.plantDataSourceNamesFr = plantDataSourceNamesFr;
		this.tagPrefix = tagPrefix;
	}

	public String getAeic() {
		return aeic;
	}
	public void setAeic(String aeic) {
		this.aeic = aeic;
	}
	public Long getUnitId() {
		return unitId;
	}
	public void setUnitId(Long unitId) {
		this.unitId = unitId;
	}
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	public UnitType getUnitType() {
		return unitType;
	}
	public void setUnitType(UnitType unitType) {
		this.unitType = unitType;
	}
	public String getDataSourceName() {
		return dataSourceName;
	}
	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}
	
	public String getDataSourceNameSoe() {
        return dataSourceNameSoe;
    }
    public void setDataSourceNameSoe(String dataSourceNameSoe) {
        this.dataSourceNameSoe = dataSourceNameSoe;
    }
    
    public String getDataSourceNameFr() {
        return dataSourceNameFr;
    }
    public void setDataSourceNameFr(String dataSourceNameFr) {
        this.dataSourceNameFr = dataSourceNameFr;
    }
    
    public String getPlantDataSourceNamesSoe() {
        return plantDataSourceNamesSoe;
    }

    public void setPlantDataSourceNamesSoe(String plantDataSourceNamesSoe) {
        this.plantDataSourceNamesSoe = plantDataSourceNamesSoe;
    }
    
    public String getPlantDataSourceNamesFr() {
        return plantDataSourceNamesFr;
    }

    public void setPlantDataSourceNamesFr(String plantDataSourceNamesFr) {
        this.plantDataSourceNamesFr = plantDataSourceNamesFr;
    }

	public String getUnitNumber() {
		return unitNumber;
	}

	public void setUnitNumber(String unitNumber) {
		this.unitNumber = unitNumber;
	}

	public String getPlantName() {
		return plantName;
	}

	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}

	public String getPlantConfig() {
		return plantConfig;
	}

	public void setPlantConfig(String plantConfig) {
		this.plantConfig = plantConfig;
	}
	
	public String getPlantDataSourceNames() {
		return plantDataSourceNames;
	}

	public void setPlantDataSourceNames(String plantDataSourceNames) {
		this.plantDataSourceNames = plantDataSourceNames;
	}
	
	public String getTagPrefix()
    {
        return tagPrefix;
    }

    public void setTagPrefix(String tagPrefix)
    {
        this.tagPrefix = tagPrefix;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((aeic == null) ? 0 : aeic.hashCode());
        result = prime * result + ((dataSourceName == null) ? 0 : dataSourceName.hashCode());
        result = prime * result + ((plantConfig == null) ? 0 : plantConfig.hashCode());
        result = prime * result + ((plantDataSourceNames == null) ? 0 : plantDataSourceNames.hashCode());
        result = prime * result + ((plantName == null) ? 0 : plantName.hashCode());
        result = prime * result + ((tagPrefix == null) ? 0 : tagPrefix.hashCode());
        result = prime * result + ((unitId == null) ? 0 : unitId.hashCode());
        result = prime * result + ((unitName == null) ? 0 : unitName.hashCode());
        result = prime * result + ((unitNumber == null) ? 0 : unitNumber.hashCode());
        result = prime * result + ((unitType == null) ? 0 : unitType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        PresentableNewUnit other = (PresentableNewUnit) obj;
        if (aeic == null)
        {
            if (other.aeic != null) return false;
        }
        else if (!aeic.equals(other.aeic)) return false;
        if (dataSourceName == null)
        {
            if (other.dataSourceName != null) return false;
        }
        else if (!dataSourceName.equals(other.dataSourceName)) return false;
        if (plantConfig == null)
        {
            if (other.plantConfig != null) return false;
        }
        else if (!plantConfig.equals(other.plantConfig)) return false;
        if (plantDataSourceNames == null)
        {
            if (other.plantDataSourceNames != null) return false;
        }
        else if (!plantDataSourceNames.equals(other.plantDataSourceNames)) return false;
        if (plantName == null)
        {
            if (other.plantName != null) return false;
        }
        else if (!plantName.equals(other.plantName)) return false;
        if (tagPrefix == null)
        {
            if (other.tagPrefix != null) return false;
        }
        else if (!tagPrefix.equals(other.tagPrefix)) return false;
        if (unitId == null)
        {
            if (other.unitId != null) return false;
        }
        else if (!unitId.equals(other.unitId)) return false;
        if (unitName == null)
        {
            if (other.unitName != null) return false;
        }
        else if (!unitName.equals(other.unitName)) return false;
        if (unitNumber == null)
        {
            if (other.unitNumber != null) return false;
        }
        else if (!unitNumber.equals(other.unitNumber)) return false;
        if (unitType != other.unitType) return false;
        return true;
    }

}
