package com.siemens.gs.it.ads.unit;

import com.siemens.gs.it.ads.core.domain.characteristic.CharacteristicType;

public interface SimpleCharacteristic
{
    Long getId();

    String getName();
    
    boolean isImported();
    
    public CharacteristicType getType();
}
