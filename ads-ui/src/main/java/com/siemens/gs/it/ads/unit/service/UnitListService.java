package com.siemens.gs.it.ads.unit.service;

import java.util.List;
import java.util.Map;

import com.siemens.gs.it.ads.core.domain.unit.UnitData;

public interface UnitListService {
	
	public UnitData getUnitDetails(Long unitId);
	
	public Map<String,List<String>> getDataSourceNamesFromSnowFlake(UnitData unit);
	
	public void getSoeAndFrDatasourceNames();
}
