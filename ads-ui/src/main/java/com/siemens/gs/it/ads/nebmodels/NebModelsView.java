package com.siemens.gs.it.ads.nebmodels;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.siemens.gs.it.ads.AdsApplicationLayout;
import com.siemens.gs.it.ads.AdsContentLayout;
import com.siemens.gs.it.ads.spring.AdsMessageSource;
import com.siemens.gs.it.ads.unit.list.UnitLayout;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.router.Route;

@Route(value = "nebmodels", layout = AdsApplicationLayout.class)
@PreserveOnRefresh
//@AllAuthenticatedUsers
public class NebModelsView extends AdsContentLayout {
	private static final long serialVersionUID = 1L;
	@Autowired
	private AdsMessageSource messageSource;

    public NebModelsView() {
    	add(new UnitLayout());
    }
    
    @PostConstruct
    private void init() {
    	setHeaderText(this.messageSource.getMessage("view.nebModels"));
    }
}