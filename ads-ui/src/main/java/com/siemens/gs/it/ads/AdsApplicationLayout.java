package com.siemens.gs.it.ads;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

import com.siemens.gs.it.ads.admin.AdminView;
import com.siemens.gs.it.ads.analytics.AnalyticsView;
import com.siemens.gs.it.ads.apptheme.component.SfIcon;
import com.siemens.gs.it.ads.apptheme.component.SfIcon.SiemensIcon;
import com.siemens.gs.it.ads.apptheme.layout.SeAppLayout;
import com.siemens.gs.it.ads.characteristics.CharacteristicsView;
import com.siemens.gs.it.ads.dataavail.DataAvailabilityView;
import com.siemens.gs.it.ads.exemodel.ExecuteModelView;
import com.siemens.gs.it.ads.maintenance.MaintenanceView;
import com.siemens.gs.it.ads.modelexe.ModelExeView;
import com.siemens.gs.it.ads.models.ModelView;
import com.siemens.gs.it.ads.nebmappings.NebMappingsView;
import com.siemens.gs.it.ads.nebmodels.NebModelsView;
import com.siemens.gs.it.ads.nebsys.NebSystemView;
import com.siemens.gs.it.ads.spring.AdsMessageSource;
import com.siemens.gs.it.ads.spring.security.ApplicationSecurity;
import com.siemens.gs.it.ads.symbolicnames.SymbNamesView;
import com.siemens.gs.it.ads.tdy.TdyInputListView;
import com.siemens.gs.it.ads.unit.detail.UnitDetailsViewImpl;
import com.siemens.gs.it.ads.unit.list.UnitViewImpl;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;

/**
 * The ADS application layout.
 */
public class AdsApplicationLayout extends SeAppLayout {
	private static final long serialVersionUID = 1L;
	
	private AdsMessageSource messageSource;
	private ApplicationSecurity applicationSecurity;
	private VersionService versionService;
	private ApplicationEventPublisher eventPublisher;
	
	@Autowired
    public AdsApplicationLayout(AdsMessageSource messageSource, ApplicationSecurity applicationSecurity, VersionService versionService, ApplicationEventPublisher eventPublisher) {
    	super();
    	this.messageSource = messageSource;
    	this.applicationSecurity = applicationSecurity;
    	this.versionService = versionService;
    	this.eventPublisher = eventPublisher;
    }

    private Component [] createNavigationButtons() {
    	Button logoutButton = createButton(this.messageSource.getMessage("navigation.logout"), SfIcon.withIcon(SiemensIcon.exit));
    	logoutButton.addClickListener(click -> {
    		click.getSource().getUI().ifPresent(ui -> {
    			ui.removeAll();
    		});
    		
    		});
    	Component [] buttons = new Component [] {
    			createButton(applicationSecurity.getCurrentUser().getGid(), SfIcon.withIcon(SiemensIcon.person)),
    			createButton(this.messageSource.getMessage("application.help"), SfIcon.withIcon(SiemensIcon.question)),
    			createButton(this.messageSource.getMessage("navigation.notifications"), SfIcon.withIcon(SiemensIcon.notification)),
    			logoutButton
    	};
        return buttons;
    }
    
    private Button createButton(String label, SfIcon icon) {
    	Button button = new Button(label);
    	button.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE, ButtonVariant.LUMO_CONTRAST);
    	button.setIcon(icon);
    	return button;
    }
    
    @PostConstruct
    private void init() {
    	ApplicationInformation applicationInformation = new ApplicationInformation("ADS", this.messageSource.getMessage("application.title"), 
    			String.format("Version: %s (%s)", this.versionService.getVersion(), this.versionService.getBuildTimestamp()));
		showApplicationInformation(applicationInformation);
    	List<MenuItemInfo> menuItems = new LinkedList<>(Arrays.asList(new MenuItemInfo[] { 
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.unit")).withView(UnitViewImpl.class).withDefaultView(true).withSubViews(Arrays.asList(UnitDetailsViewImpl.class)),
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.characteristic")).withView(CharacteristicsView.class),
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.symbolicName")).withView(SymbNamesView.class),
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.modelList")).withView(ModelView.class),
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.dataAvailability")).withView(DataAvailabilityView.class),
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.executeRules")).withView(ExecuteModelView.class),
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.ruleExecutions")).withView(ModelExeView.class),
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.maintinance.tasks")).withView(MaintenanceView.class),
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.workbench")).withView(AnalyticsView.class),
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.admin.tasks")).withView(AdminView.class),
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.tdyInputList")).withView(TdyInputListView.class),
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.nebSystems")).withView(NebSystemView.class),
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.nebMapping")).withView(NebMappingsView.class),
    			MenuItemInfo.create().withText(this.messageSource.getMessage("view.nebModels")).withView(NebModelsView.class),
        }));
    	//if (VaadinSecurity.hasAccessTo(AdminView.class)) {
    	//}
    		
    	showSecondaryNavigation(menuItems);
    	showAdditionalContent(createNavigationButtons());
    	UI.getCurrent().getPage().addBrowserWindowResizeListener(event -> {
			eventPublisher.publishEvent(event);
		});
    }
}