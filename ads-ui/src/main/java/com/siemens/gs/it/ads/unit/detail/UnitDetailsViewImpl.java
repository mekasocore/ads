package com.siemens.gs.it.ads.unit.detail;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import com.google.common.collect.Sets;
import com.siemens.gs.it.ads.AdsApplicationLayout;
import com.siemens.gs.it.ads.AdsContentLayout;
import com.siemens.gs.it.ads.core.domain.DataSource;
import com.siemens.gs.it.ads.core.domain.unit.UnitData;
import com.siemens.gs.it.ads.spring.AdsMessageSource;
import com.siemens.gs.it.ads.unit.detail.UnitDetailsLayout.DataSourceLayout;
import com.siemens.gs.it.ads.unit.list.UnitView.LoadUnitEvent;
import com.siemens.gs.it.ads.unit.list.UnitViewImpl;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.router.Route;

@Route(value = "unit/:unitId", layout = AdsApplicationLayout.class)
@PreserveOnRefresh
@Component
//@AllAuthenticatedUsers
public class UnitDetailsViewImpl extends AdsContentLayout implements BeforeEnterObserver, UnitDetailsView {
	private static final long serialVersionUID = 1L;
	private UnitDetailsLayout layout;
	private AdsMessageSource messageSource;
	private transient ApplicationEventPublisher eventPublisher;
	
	private UnitData unitData;
	
	@Autowired
	public UnitDetailsViewImpl(AdsMessageSource messageSource, ApplicationEventPublisher eventPublisher) {
		this.messageSource = messageSource;
		this.eventPublisher = eventPublisher;
		this.layout = new UnitDetailsLayout();
		add(this.layout);
	}
	
	@PostConstruct
	private void init() {
		this.layout.getSaveButton().setText(this.messageSource.getMessage("action.save"));
		this.layout.getCancelButton().setText(this.messageSource.getMessage("action.cancel"));
		this.layout.getDeleteButton().setText(this.messageSource.getMessage("action.delete"));
		
		this.layout.getDatasourceTab().getLabelSpan().setText(this.messageSource.getMessage("view.unit.datasource.configuration"));
		this.layout.getCharacteristicsTab().getLabelSpan().setText(this.messageSource.getMessage("view.characteristicForm.title"));
		this.layout.getTagMappingTab().getLabelSpan().setText(this.messageSource.getMessage("view.symbolicNameDetails.tagMapping"));
		this.layout.getAssignedModelsTab().getLabelSpan().setText(this.messageSource.getMessage("view.unit.assignedRules"));
		this.layout.getSuggestedModelsTab().getLabelSpan().setText(this.messageSource.getMessage("view.unit.suggestedRules"));
		
		this.layout.getCancelButton().addClickListener(click -> click.getSource().getUI().ifPresent(ui -> {
			ui.navigate(UnitViewImpl.class);
		}));
		this.layout.getTabs().addSelectedChangeListener(select -> {
			Tab selectedTab = select.getSelectedTab();
			this.layout.clear();
			if (selectedTab.equals(this.layout.getDatasourceTab())) {
				this.eventPublisher.publishEvent(new LoadDataSourceNamesEvent(this.unitData));
			} else if (selectedTab.equals(this.layout.getCharacteristicsTab())) {
				
			} else if (selectedTab.equals(this.layout.getTagMappingTab())) {
				
			} else if (selectedTab.equals(this.layout.getAssignedModelsTab())) {
				
			} else if (selectedTab.equals(this.layout.getSuggestedModelsTab())) {
				
			}
		});
	}
	
	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		event.getRouteParameters().get("unitId").ifPresent(unitId -> {
			try {
				Long lUnitId = Long.valueOf(unitId);
				this.eventPublisher.publishEvent(new LoadUnitEvent(lUnitId));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		});
		this.eventPublisher.publishEvent(new LoadDataSourceNamesEvent(this.unitData));
	}
	
	public void populateView(UnitData unitData) {
		this.unitData = unitData;
		setHeaderText(String.format("%s %s", this.messageSource.getMessage("view.unit.editUnit"), unitData.getName()));
	}
	
	public void showDatasourceNames(Map<String, List<String>> dataSourceMap) {
		DataSourceLayout dataSourceLayout = this.layout.getDataSourceLayout();
		
		dataSourceLayout.wts.setLabel(this.messageSource.getMessage("view.kksMapping.datasource.wts.name.selection"));
		dataSourceLayout.wts.setItems(dataSourceMap.get(DataSource.WIN_TS.name()));
		if (StringUtils.isNoneEmpty(this.unitData.getUnitMetaData().getDatasourceName())) {
			dataSourceLayout.wts.setValue(Sets.newHashSet(StringUtils.split(this.unitData.getUnitMetaData().getDatasourceName(), ',')));
		}
		
		dataSourceLayout.soe.setLabel(this.messageSource.getMessage("view.kksMapping.datasource.soe.name.selection"));
		dataSourceLayout.soe.setItems(dataSourceMap.get(DataSource.SOE.name()));
		if (StringUtils.isNoneEmpty(this.unitData.getUnitMetaData().getDatasourceNameSoe())) {
			dataSourceLayout.soe.setValue(Sets.newHashSet(StringUtils.split(this.unitData.getUnitMetaData().getDatasourceNameSoe(), ',')));
		}
		dataSourceLayout.fr.setLabel(this.messageSource.getMessage("view.kksMapping.datasource.fr.name.selection"));
		dataSourceLayout.fr.setItems(dataSourceMap.get(DataSource.FR.name()));
		if (StringUtils.isNoneEmpty(this.unitData.getUnitMetaData().getDatasourceNameFr())) {
			dataSourceLayout.fr.setValue(Sets.newHashSet(StringUtils.split(this.unitData.getUnitMetaData().getDatasourceNameFr(), ',')));
		}
	}
}
