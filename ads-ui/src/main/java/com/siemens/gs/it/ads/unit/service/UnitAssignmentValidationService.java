package com.siemens.gs.it.ads.unit.service;

import java.util.List;

import com.siemens.gs.it.ads.core.Model;

public interface UnitAssignmentValidationService {
    public void reCalculateAndUpdateValidationStatus(List<Model> modelList);
}