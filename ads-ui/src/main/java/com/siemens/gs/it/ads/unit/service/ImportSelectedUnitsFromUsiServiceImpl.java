package com.siemens.gs.it.ads.unit.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.siemens.gs.it.ads.core.domain.EntityStatus;
import com.siemens.gs.it.ads.core.domain.unit.UnitData;
import com.siemens.gs.it.ads.core.domain.unit.UnitDetails;
import com.siemens.gs.it.ads.core.repository.unit.UnitDataRepository;
import com.siemens.gs.it.ads.core.service.unit.impl.ImportPropertiesService;
import com.siemens.gs.it.ads.core.service.unit.impl.ImportedUnitData;


@Service
public class ImportSelectedUnitsFromUsiServiceImpl implements ImportSelectedUnitsFromUsiService{
	
	private final static Logger LOG = LoggerFactory.getLogger(ImportSelectedUnitsFromUsiServiceImpl.class);
	
	private javax.sql.DataSource sensorReadingsDataSource;
	private UnitDataRepository unitDataRepository;
	private final ImportPropertiesService importUsiPropertiesUseCase;
	private int count;
	private int errorCount;
	
	@Autowired
	public ImportSelectedUnitsFromUsiServiceImpl(@Qualifier("sensorReadingsDataSource") DataSource sensorReadingsDataSource, UnitDataRepository unitDataRepository,
			ImportPropertiesService importUsiPropertiesUseCase) {
		this.sensorReadingsDataSource = sensorReadingsDataSource;
		this.unitDataRepository = unitDataRepository;
		this.importUsiPropertiesUseCase = importUsiPropertiesUseCase;
	}
	
	
	/**
	 *  Funtion to import metadata of selected units from USI
	 */
	@Override
	public Response importSelectedUnitsFromUsi(Set<UnitDetails> unitDetailsSet) {
		count = 0;
		errorCount = 0;

		try {
			NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(sensorReadingsDataSource);
			Set<String> aeicSet = unitDetailsSet.stream().map(UnitDetails::getAeic).collect(Collectors.toSet());
			;

			String queryForNewSelectedUnits = "SELECT * FROM SVC_ADS.USI_METADATA WHERE unit_type IN ('CT', 'ST') AND AEIC IN (:aeicSet)";
			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("aeicSet", aeicSet);

			LOG.info("Now querying for all units with " + queryForNewSelectedUnits);

			jdbcTemplate.query(queryForNewSelectedUnits, paramMap, ImportedUnitData::map)
					.forEach(unit -> importNewSingleUnit(unit));
		} catch (Exception ex) {
			LOG.error("Failed to import selected Unit from USI due to ", ex);
			throw new RuntimeException(ex);

		}
		return new Response(count, errorCount, 0);
	}
	
	
	private void importNewSingleUnit(ImportedUnitData importedUnit) {
		try {
			UnitData localUnit = unitDataRepository.findById(importedUnit.getUnitId())
					.orElse(new UnitData(importedUnit.getUnitId()));
			localUnit.setName(importedUnit.getName());
			localUnit.setUnitMetaData(importedUnit.getUnitMetaData());
			localUnit.getUnitMetaData().setDatasourceName(importedUnit.getUnitMetaData().getDatasourceName());
			localUnit.getUnitMetaData().setAeic(importedUnit.getUnitMetaData().getAeic());
			localUnit.getUnitMetaData().setPkz(importedUnit.getUnitMetaData().getPkz());
			localUnit.getUnitMetaData().setPlantFlId(importedUnit.getUnitMetaData().getPlantFlId());
			localUnit.getUnitMetaData().setUnitFlId(importedUnit.getUnitMetaData().getUnitFlId());
			if (!EntityStatus.Active.equals(localUnit.getEntityStatus())) {
				localUnit.setEntityStatus(EntityStatus.Active);
				LOG.info("Unit " + localUnit.getName() + " (" + localUnit.getUnitMetaData().getAeic()
						+ ") has been activated in ADS because sensor data was found.");
			} 

			localUnit = unitDataRepository.save(localUnit);

			if (EntityStatus.Active.equals(localUnit.getEntityStatus())) {
				importUsiPropertiesUseCase.importCharacteristics(importedUnit, localUnit);
				count++;
			}

			LOG.info("Unit " + importedUnit + " imported from USI.");

		} catch (Exception ex) {
			LOG.error("Unit " + importedUnit + " caused an error on import. ", ex);
			errorCount++;
		}
	}
	
	

}
