package com.siemens.gs.it.ads.unit.detail;

import java.util.List;
import java.util.Map;

import com.siemens.gs.it.ads.core.domain.unit.UnitData;

public interface UnitDetailsView {
	public void populateView(UnitData unit);
	public void showDatasourceNames(Map<String, List<String>> dataSourceMap);
	
	
	public static class LoadDataSourceNamesEvent {
		private UnitData unitData;
		public LoadDataSourceNamesEvent(UnitData unitData) {
			this.unitData = unitData;
		}
		public UnitData getUnitData() {
			return unitData;
		}
	}
}
