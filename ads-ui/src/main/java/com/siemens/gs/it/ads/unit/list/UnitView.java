package com.siemens.gs.it.ads.unit.list;

import java.util.List;
import java.util.Set;

import com.siemens.gs.it.ads.core.domain.characteristic.Characteristic;
import com.siemens.gs.it.ads.core.domain.unit.UnitData;
import com.siemens.gs.it.ads.core.domain.user.AdsUser;
import com.siemens.gs.it.ads.unit.PresentableUnit;


public interface UnitView {
	public void populateUnitTable(List<PresentableUnit> items);
	public void addUnits();
	public void showUnitCharacteristicsDialog(List<Characteristic> characteristics);
	
	public static class ConfigureColumnsEvent {
	    public ConfigureColumnsEvent() { }
	}
	
	public static class AddFavoriteEvent {
		private AdsUser adsUser;
		private UnitData unitData;
	    public AddFavoriteEvent(AdsUser adsUser, UnitData unitData) {
	    	this.adsUser = adsUser;
	    	this.unitData = unitData;
	    }
		public AdsUser getAdsUser() {
			return adsUser;
		}
		public UnitData getUnitData() {
			return unitData;
		}
	}
	
	public static class RemoveFavoriteEvent extends AddFavoriteEvent {

		public RemoveFavoriteEvent(AdsUser adsUser, UnitData unitData) {
			super(adsUser, unitData);
		}
	}
	
	
	public static class AddUnitsEvent {
		private List<String> unitNames;

	    public AddUnitsEvent(List<String> unitNames) {
	        this.unitNames = unitNames;
	    }

	    public List<String> getUnitNames() {
	        return unitNames;
	    }
	}
	public static class LoadUnitsEvent {
		private Set<String> visibleColumns;
		private AdsUser adsUser;
		public LoadUnitsEvent(Set<String> visibleColumns, AdsUser adsUser) {
			this.visibleColumns = visibleColumns;
			this.adsUser = adsUser;
		}
		public Set<String> getVisibleColumns() {
			return visibleColumns;
		}
		public AdsUser getAdsUser() {
			return adsUser;
		}
	}
	
	public static class LoadUnitEvent {
		private Long unitId;
		public LoadUnitEvent(Long unitId) {
			this.unitId = unitId;
		}
		public Long getUnitId() {
			return unitId;
		}
	}
}
