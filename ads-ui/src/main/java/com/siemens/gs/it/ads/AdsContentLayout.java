package com.siemens.gs.it.ads;


import com.siemens.gs.it.ads.apptheme.layout.SeContentLayout;

public class AdsContentLayout extends SeContentLayout {
	private static final long serialVersionUID = 1L;

	@Override
	public String getPageTitle() {
		return "Advanced Diagnostic System - " + super.getPageTitle();
	}
}