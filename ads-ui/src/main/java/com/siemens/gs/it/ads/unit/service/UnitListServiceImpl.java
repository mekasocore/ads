package com.siemens.gs.it.ads.unit.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import com.siemens.gs.it.ads.core.domain.DataSource;
import com.siemens.gs.it.ads.core.domain.unit.UnitData;
import com.siemens.gs.it.ads.core.domain.unit.UnitDetails;
import com.siemens.gs.it.ads.core.repository.unit.UnitDataRepository;
import com.siemens.gs.it.ads.core.service.kksmapping.GenerateKksMappingsForUnitUseCase;
import com.siemens.gs.it.ads.core.service.usernotification.NotifyUserAboutEventsUseCase;
import com.siemens.gs.it.ads.spring.security.ApplicationSecurity;


@Service
@Scope("prototype")
public class UnitListServiceImpl implements UnitListService{
	
	private static final Logger LOG = LoggerFactory.getLogger(UnitListServiceImpl.class);
	
	private final javax.sql.DataSource sensorReadingsDataSource;
    private UnitDataRepository unitDataRepository;	
    private ImportSelectedUnitsFromUsiService importSelectedUnitsFromUsiService;

    private GenerateKksMappingsForUnitUseCase generateKksMappingsForUnitUseCase;
	private NotifyUserAboutEventsUseCase notifyUserAboutEventsUseCase;

	private ApplicationSecurity applicationSecurity;

	private final String SINGLE_SHAFT = "1S";
	private final javax.sql.DataSource dataSource;
	
    private List<String> dataSourceNamesWts ;
    private List<String> dataSourceNamesSoe ;
    private List<String> dataSourceNamesFr ;
 	
    @Autowired
	public UnitListServiceImpl(@Qualifier("sensorReadingsDataSource") javax.sql.DataSource sensorReadingsDataSource, @Qualifier("dataSource") javax.sql.DataSource dataSource,UnitDataRepository unitDataRepository,
			ImportSelectedUnitsFromUsiService importSelectedUnitsFromUsiService,GenerateKksMappingsForUnitUseCase generateKksMappingsForUnitUseCase,NotifyUserAboutEventsUseCase notifyUserAboutEventsUseCase,ApplicationSecurity applicationSecurity) {
		this.sensorReadingsDataSource = sensorReadingsDataSource;
		this.unitDataRepository = unitDataRepository;
		this.importSelectedUnitsFromUsiService = importSelectedUnitsFromUsiService;
		this.generateKksMappingsForUnitUseCase = generateKksMappingsForUnitUseCase;
		this.notifyUserAboutEventsUseCase = notifyUserAboutEventsUseCase;
		this.applicationSecurity = applicationSecurity;
		this.dataSource = dataSource;
	}
    
	@Override
	public Map<String,List<String>> getDataSourceNamesFromSnowFlake(UnitData unit){
		
	    Map<String,List<String>> dataSourceNamesMap = new HashMap<>();
	    
		NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(sensorReadingsDataSource);

		final String queryToLookupView = "select  PLANT_DATASOURCE_NAMES , PLANT_DATASOURCE_NAMES_SOE,PLANT_DATASOURCE_NAMES_FR from svc_ads.ads_avail_units where aeic = :aeic";
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("aeic", unit.getUnitMetaData().getAeic());
		
		try {
			jdbcTemplate.query(queryToLookupView, paramMap, rs -> {
			    dataSourceNamesWts = parseStringArrayOfDataSourceNames(rs.getString("PLANT_DATASOURCE_NAMES"));
			    dataSourceNamesSoe = parseStringArrayOfDataSourceNames(rs.getString("PLANT_DATASOURCE_NAMES_SOE"));
			    dataSourceNamesFr  = parseStringArrayOfDataSourceNames(rs.getString("PLANT_DATASOURCE_NAMES_FR"));
			});
			
			dataSourceNamesMap.put(DataSource.WIN_TS.name(), dataSourceNamesWts.stream().distinct().collect(Collectors.toList()));
			dataSourceNamesMap.put(DataSource.SOE.name(), dataSourceNamesSoe.stream().distinct().collect(Collectors.toList()));
			dataSourceNamesMap.put(DataSource.FR.name(), dataSourceNamesFr.stream().distinct().collect(Collectors.toList()));

			} catch (Exception ex) {
				LOG.error("Failed to get datasource names from lookup view due to ",ex);
				
				
				dataSourceNamesMap.put(DataSource.WIN_TS.name(), Arrays.asList("1", "2", "3"));
				dataSourceNamesMap.put(DataSource.SOE.name(), Arrays.asList("4", "5", "6"));
				dataSourceNamesMap.put(DataSource.FR.name(), Arrays.asList("7", "8", "9"));
				
				//throw new RuntimeException("Error while fetching datasource names ", ex.getCause()); 
			}
		
		return dataSourceNamesMap;
	}
	
	
	@Override
	public void getSoeAndFrDatasourceNames(){
		
	    Map<String, UnitDetails> unitDetailsMap = new HashMap<>();
	    
		NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(sensorReadingsDataSource);

		final String queryToLookupViewForSoe = "select aeic, case when contains(PLANT_DATASOURCE_NAMES_SOE, DATASOURCE_NAME) then DATASOURCE_NAME || '_SOE' else null end as selected_datasource_name_soe\r\n" + 
				"from \"MOSAIC\".\"SVC_ADS\".\"ADS_AVAIL_UNITS\"\r\n" + 
				"where DATASOURCE_NAME is not null and selected_datasource_name_soe is not null\r\n" + 
				"order by unit_name";
		
		final String queryToLookupViewForFr = "select aeic, case when contains(PLANT_DATASOURCE_NAMES_FR, DATASOURCE_NAME) then DATASOURCE_NAME || '_FR' else null end as selected_datasource_name_fr\r\n" + 
				"from \"MOSAIC\".\"SVC_ADS\".\"ADS_AVAIL_UNITS\"\r\n" + 
				"where DATASOURCE_NAME is not null and selected_datasource_name_fr is not null\r\n" + 
				"order by unit_name";
		
		try {
			jdbcTemplate.query(queryToLookupViewForSoe, rs -> {
				unitDetailsMap.put(rs.getString("AEIC"),prepareUnitDetailsForSoe(rs.getString("AEIC"),rs.getString("SELECTED_DATASOURCE_NAME_SOE")));
			});
			
			} catch (Exception ex) {
				LOG.error("Failed to get datasource names from lookup view due to ",ex); 
				throw new RuntimeException("Error while fetching datasource names ", ex.getCause()); 
			}
		
		
		try {
			jdbcTemplate.query(queryToLookupViewForFr, rs -> {
				prepareUnitDetailsForFr(unitDetailsMap,rs.getString("AEIC"),rs.getString("SELECTED_DATASOURCE_NAME_FR"));
			});
			
			} catch (Exception ex) {
				LOG.error("Failed to get datasource names from lookup view due to ",ex); 
				throw new RuntimeException("Error while fetching datasource names ", ex.getCause()); 
			}
		
		unitDetailsMap.entrySet().stream().forEach(e -> {
			updateDatasourceName(e);
		});

		
	}
	
	
	
	private void updateDatasourceName(Map.Entry<String, UnitDetails> unitDetailsMapEntry) {
		try {
			UnitData localUnit = unitDataRepository.findUnitByAeic(unitDetailsMapEntry.getKey());
					
			localUnit.getUnitMetaData().setDatasourceNameFr(unitDetailsMapEntry.getValue().getDataSourceNameFr());
			localUnit.getUnitMetaData().setDatasourceNameSoe(unitDetailsMapEntry.getValue().getDataSourceNameSoe());
			localUnit = unitDataRepository.save(localUnit);
			
			LOG.info("SOE and FR Datasource names got updated for unit "+localUnit.getName());

		} catch (Exception ex) {
			LOG.error("Unit with AEIC" + unitDetailsMapEntry.getKey() + " caused an error on import. ", ex);
			
		}
	}
	
	private List<String> parseStringArrayOfDataSourceNames(String dataSourceNames){
		List<String> dataSourceNamesList = new ArrayList<>() ;
		if(null != dataSourceNames && !dataSourceNames.isEmpty()) {
				if(dataSourceNames.contains(",")) {
					dataSourceNamesList = Arrays.asList(dataSourceNames.split(","));
				} else {
					dataSourceNamesList= Arrays.asList(dataSourceNames);
				}
			} else {
				LOG.info("No datasource names found for this unit ");
			}
		return dataSourceNamesList;
	}
	
	
	
	private UnitDetails prepareUnitDetailsForSoe(String aeic,String dataSourceNameSoe){
		UnitDetails unitDetails = null;
		if(null != dataSourceNameSoe && !dataSourceNameSoe.isEmpty()) {
				unitDetails = new UnitDetails(aeic, dataSourceNameSoe, null);
			} else {
				LOG.info("No datasource name found ");
			}
		return unitDetails;
	}
	
	
	private Map<String,UnitDetails> prepareUnitDetailsForFr(Map<String,UnitDetails> unitDetailsMap,String aeic,String dataSourceNameFr){
		if(unitDetailsMap.keySet().contains(aeic)) {
			unitDetailsMap.get(aeic).setDataSourceNameFr(dataSourceNameFr);
		}else {
			unitDetailsMap.put(aeic, new UnitDetails(aeic, null, dataSourceNameFr));
		}
		
		
	return unitDetailsMap;
		
		
	}
	
	@Override
	public UnitData getUnitDetails(Long unitId){
		if(unitDataRepository.findById(unitId).isPresent())
			return unitDataRepository.findById(unitId).get();
		else
			return null;
	}
	
	
	private String getDataSourceNamesUsingKksCodeInformation(UnitDetails unitDetails){

	    StringBuilder dataSourceNamesInOneStringBuilder = new StringBuilder();
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(sensorReadingsDataSource);

        StringBuilder queryToLookUpView = new StringBuilder();
        queryToLookUpView.append("select split_part(DATASOURCE, '_', 0) as DATASOURCE_NAME,").append("\r\n");
        queryToLookUpView.append("case split_part(PDC_SNSR_STATS.DATASOURCE, '_', 2)").append("\r\n");
        queryToLookUpView.append("when '' then 'WIN_TS'").append("\r\n");
        queryToLookUpView.append("when 'SOE' then 'SOE'").append("\r\n");
        queryToLookUpView.append("when 'FR' then 'FR'").append("\r\n");
        queryToLookUpView.append("end as DATASOURCE_TYPE").append("\r\n");
        queryToLookUpView.append("from SRC_INT_PDC.PDC_SNSR_STATS").append("\r\n");
        queryToLookUpView.append("where CONTAINS(tag_name, :tagPrefix) and PDC_SNSR_STATS.DATASOURCE in (select r.value from table(split_to_table(:dataSourceNames, ',')) r)").append("\r\n");
        queryToLookUpView.append("and PDC_SNSR_STATS.DATASOURCE rlike '[[:alnum:]]+(|_SOE|_FR)'and DATASOURCE_TYPE = 'WIN_TS'").append("\r\n");
        queryToLookUpView.append("group by 1, 2").append("\r\n");
        queryToLookUpView.append("order by 2 desc, 1").append(";");
        
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("tagPrefix", unitDetails.getTagPrefix());
        paramMap.put("dataSourceNames", unitDetails.getPlantDataSourceNames());
        
       try {
               
              jdbcTemplate.query(queryToLookUpView.toString(), paramMap, rs -> {
                    dataSourceNamesInOneStringBuilder.append(rs.getString("DATASOURCE_NAME"));
                    dataSourceNamesInOneStringBuilder.append(",");
            });
              
              if(!dataSourceNamesInOneStringBuilder.toString().isEmpty())
              dataSourceNamesInOneStringBuilder.replace(dataSourceNamesInOneStringBuilder.toString().lastIndexOf(","), dataSourceNamesInOneStringBuilder.toString().lastIndexOf(",")+1, "");
              

            } catch (Exception ex) {
                LOG.error("Failed to get datasource names from lookup view due to ",ex); 
                throw new RuntimeException("Error while fetching datasource names ", ex.getCause()); 
            }
    return dataSourceNamesInOneStringBuilder.toString();
       
	}

}
