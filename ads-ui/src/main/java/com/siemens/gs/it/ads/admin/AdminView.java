package com.siemens.gs.it.ads.admin;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.siemens.gs.it.ads.AdsApplicationLayout;
import com.siemens.gs.it.ads.AdsContentLayout;
import com.siemens.gs.it.ads.spring.AdsMessageSource;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.router.Route;

@Route(value = "admin", layout = AdsApplicationLayout.class)
@PreserveOnRefresh
//@RequiresAdminRole
public class AdminView extends AdsContentLayout {
	private static final long serialVersionUID = 1L;
	
	private AdsMessageSource messageSource;
	
	@Autowired
    public AdminView(AdsMessageSource messageSource) {
		super();
		this.messageSource = messageSource;
    }
    
    @PostConstruct
    private void init() {
    	setHeaderText(this.messageSource.getMessage("view.admin.tasks"));
    }

}
