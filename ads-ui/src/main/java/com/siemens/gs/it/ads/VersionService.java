package com.siemens.gs.it.ads;


import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("ads")
public class VersionService
{
    private String version;

    private String buildTimestamp;

    public String getDisplayedVersionHtml()
    {
        return String.format("<span class='version'>%s</span> " //
                             + "<span class='build-time'>%s</span>", //
                             version, buildTimestampAsString());
    }

    private String buildTimestampAsString()
    {
        return ObjectUtils.isEmpty(buildTimestamp) ? "" : ZonedDateTime.parse(buildTimestamp).format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM));
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getBuildTimestamp()
    {
        return buildTimestamp;
    }

    public void setBuildTimestamp(String buildTimestamp)
    {
        this.buildTimestamp = buildTimestamp;
    }

}
