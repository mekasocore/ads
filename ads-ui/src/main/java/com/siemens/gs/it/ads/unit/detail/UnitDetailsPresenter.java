package com.siemens.gs.it.ads.unit.detail;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;

import com.siemens.gs.it.ads.core.domain.unit.UnitData;
import com.siemens.gs.it.ads.core.service.unit.ReadUnitDataUseCase;
import com.siemens.gs.it.ads.unit.detail.UnitDetailsView.LoadDataSourceNamesEvent;
import com.siemens.gs.it.ads.unit.list.UnitView.LoadUnitEvent;
import com.siemens.gs.it.ads.unit.service.UnitListService;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.VaadinSessionScope;

@SpringComponent
@VaadinSessionScope
public class UnitDetailsPresenter {
	private transient UnitDetailsView view;
	private transient ReadUnitDataUseCase readUnitDataUseCase;
	private transient UnitListService unitListService;
	
	@Autowired
	public UnitDetailsPresenter(UnitDetailsView view, ReadUnitDataUseCase readUnitDataUseCase, UnitListService unitListService) {
		this.view = view;
		this.readUnitDataUseCase = readUnitDataUseCase;
		this.unitListService = unitListService;
	}
	
	@EventListener
	public void loadUnits(LoadUnitEvent event) {
		UnitData unitData = this.readUnitDataUseCase.byId(event.getUnitId());
		this.view.populateView(unitData);
	}
	
	@EventListener
	public void loadDataSourceNames(LoadDataSourceNamesEvent event) {
		Map<String, List<String>> dataSourceMap = this.unitListService.getDataSourceNamesFromSnowFlake(event.getUnitData());
		this.view.showDatasourceNames(dataSourceMap);
	}
}
