package com.siemens.gs.it.ads.unit.list;

import org.apache.commons.lang3.StringUtils;

import com.siemens.gs.it.ads.unit.PresentableUnit;
import com.vaadin.flow.component.grid.dataview.GridListDataView;

public class PresentableUnitFilter {
	private final GridListDataView<PresentableUnit> dataView;

    private String aeic;
    private String unitName;

    public PresentableUnitFilter(GridListDataView<PresentableUnit> dataView) {
        this.dataView = dataView;
        this.dataView.addFilter(this::filter);
    }

    public void setAeic(String aeic) {
        this.aeic = aeic;
        this.dataView.refreshAll();
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
        this.dataView.refreshAll();
    }

    public boolean filter(PresentableUnit presentableUnit) {
        boolean matchesAeic = matches(presentableUnit.getUnit().getUnitMetaData().getAeic(), aeic);
        boolean matchesUnitName = matches(presentableUnit.getName(), unitName);
        return matchesAeic && matchesUnitName;
    }

    private boolean matches(String value, String searchTerm) {
        return searchTerm == null || searchTerm.isEmpty() || StringUtils.containsIgnoreCase(value, searchTerm);
    }
}