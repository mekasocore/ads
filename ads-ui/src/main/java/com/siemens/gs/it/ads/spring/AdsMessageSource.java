package com.siemens.gs.it.ads.spring;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class AdsMessageSource {

	@Autowired
	private MessageSource messageSource;
	
	public String getMessage(String key) {
		return this.messageSource.getMessage(key, null, Locale.ENGLISH);
	}
	
	public String getMessage(String key, @Nullable Object[] args) {
		return this.messageSource.getMessage(key, args, Locale.ENGLISH);
	}
}
