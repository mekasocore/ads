package com.siemens.gs.it.ads.unit.service;


import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.siemens.gs.it.ads.core.Model;
import com.siemens.gs.it.ads.core.ModelVersion;
import com.siemens.gs.it.ads.core.domain.unit.UnitData;
import com.siemens.gs.it.ads.core.repository.rule.ModelVersionRepository;
import com.siemens.gs.it.ads.core.service.rule.RuleConfigurationError;
import com.siemens.gs.it.ads.core.service.rule.ValidateRuleForUnitUseCase;
import com.siemens.gs.it.ads.core.service.unit.ModelUnitMappingUseCase;

@Service
public class UnitAssignmentValidationServiceImpl implements UnitAssignmentValidationService
{

    ModelUnitMappingUseCase modelUnitMappingUseCase;
    ModelVersionRepository modelVersionRepository;
    ValidateRuleForUnitUseCase validateRuleForUnitUseCase;
    
    
    
    @Autowired
    UnitAssignmentValidationServiceImpl(ModelUnitMappingUseCase modelUnitMappingUseCase,ModelVersionRepository modelVersionRepository, ValidateRuleForUnitUseCase validateRuleForUnitUseCase){
        
        this.modelUnitMappingUseCase = modelUnitMappingUseCase;
        this.modelVersionRepository = modelVersionRepository;
        this.validateRuleForUnitUseCase = validateRuleForUnitUseCase;
    }
    
    
    @Override
    public void reCalculateAndUpdateValidationStatus(List<Model> modelList)
    {
       modelList.forEach(m ->  {
           
           List<UnitData> assignedUnits = modelUnitMappingUseCase.getMappedUnitsForModel(m);
           Optional<ModelVersion> activeModelVersion = modelVersionRepository.findActiveModelVersion(m.getId());
           
           ModelVersion modelVersion = modelVersionRepository.getWithRule(activeModelVersion.get().getId());
           List<RuleConfigurationError> validationResult = validate(modelVersion, assignedUnits);
           modelUnitMappingUseCase.updateValidationStatus(validationResult, m);
       });
        
    }
    
    private List<RuleConfigurationError> validate(ModelVersion modelVersion, List<UnitData> units) {
        Map<ModelVersion, List<UnitData>> validationInput = new HashMap<>();
        validationInput.put(modelVersion, units);
        Map<ModelVersion, List<RuleConfigurationError>> validationErrorMap = validateRuleForUnitUseCase.validate(validationInput);
        if(validationErrorMap.isEmpty())
            return Collections.emptyList();
        else
           return validationErrorMap.get(modelVersion); 
    }
    
}
