package com.siemens.gs.it.ads.spring.security;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.siemens.gs.it.ads.core.domain.user.AdsUser;

public class UserDetailsAdapter implements UserDetails {

	private static final long serialVersionUID = 1L;

	private AdsUser user;
	private Set<GrantedAuthority> grantedAuthorities;

	public UserDetailsAdapter(AdsUser user, Collection<GrantedAuthority> additionalAuthorities) {
		super();
		this.user = user;
		this.grantedAuthorities = new HashSet<>();

		if (additionalAuthorities != null) {
			this.grantedAuthorities.addAll(additionalAuthorities);
		}
	}

	public AdsUser getUser() {
		return user;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return Collections.unmodifiableCollection(grantedAuthorities);
	}

	@JsonIgnore
	@Override
	public String getPassword() {
		return "N/A";
	}

	@Override
	public String getUsername() {
		return user.getGid();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}