package com.siemens.gs.it.ads.unit;


import com.siemens.gs.it.ads.core.domain.unit.UnitData;
import com.siemens.gs.it.ads.core.domain.characteristic.UnitCharacteristicWithValue;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PresentableUnit implements Serializable
{
	private static final long serialVersionUID = 1L;


	public PresentableUnit(UnitData unit, boolean favorite, List<UnitCharacteristicWithValue> cv)
    {
        this.unit = unit;
        this.favorite = favorite;
        this.valuesByCharacteristicId = cv.stream().collect(Collectors.toMap(UnitCharacteristicWithValue::getCharacteristicId, UnitCharacteristicWithValue::getValue));
    }

    private UnitData unit;

    private boolean favorite;

    private Map<Long, String> valuesByCharacteristicId;
    private Map<Long, String> newValueByCharacteristicsMap = new HashMap<Long, String>();
 

	public Map<Long, String> getNewValueByCharacteristicsMap() {
		return newValueByCharacteristicsMap;
	}

	public void setNewValueByCharacteristicsMap(Map<Long, String> newValueByCharacteristicsMap) {
		this.newValueByCharacteristicsMap = newValueByCharacteristicsMap;
	}

	public void setValuesByCharacteristicId(Map<Long, String> valuesByCharacteristicId) {
		this.valuesByCharacteristicId = valuesByCharacteristicId;
	}

	public boolean isFavorite()
    {
        return favorite;
    }

    public void setFavorite(boolean favorite)
    {
        this.favorite = favorite;
    }

    public String getName()
    {
        return unit.getName();
    }

    public UnitData getUnit()
    {
        return unit;
    }

    public Map<Long, String> getValuesByCharacteristicId()
    {
        return valuesByCharacteristicId;
    }
    
}
