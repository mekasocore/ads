package com.siemens.gs.it.ads.unit.service;

import java.util.Set;

import com.siemens.gs.it.ads.core.domain.unit.UnitDetails;

public interface ImportSelectedUnitsFromUsiService {
	
	Response importSelectedUnitsFromUsi(Set<UnitDetails> unitDetails);
	
    class Response {
        private int count;
        private int errorCount;
        private int warningCount;

        public Response(int count, int errorCount, int warningCount) {
            this.count = count;
            this.errorCount = errorCount;
            this.warningCount = warningCount;
        }

        public int getErrorCount()
        {
            return errorCount;
        }

        public int getCount()
        {
            return count;
        }

        public int getWarningCount()
        {
            return warningCount;
        }
    }
}
