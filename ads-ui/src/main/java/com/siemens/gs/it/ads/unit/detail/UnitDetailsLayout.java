package com.siemens.gs.it.ads.unit.detail;

import org.vaadin.gatanaso.MultiselectComboBox;

import com.siemens.gs.it.ads.apptheme.component.SeCancelButton;
import com.siemens.gs.it.ads.apptheme.component.SeDeleteButton;
import com.siemens.gs.it.ads.apptheme.component.SeSaveButton;
import com.siemens.gs.it.ads.apptheme.component.SeTab;
import com.siemens.gs.it.ads.apptheme.component.SfIcon;
import com.siemens.gs.it.ads.apptheme.component.SfIcon.SiemensIcon;
import com.siemens.gs.it.ads.apptheme.layout.WithStyle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.listbox.MultiSelectListBox;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;

public class UnitDetailsLayout extends FlexLayout implements WithStyle {

	private static final long serialVersionUID = 1L;
	
	private Button cancelButton;
	private Button saveButton;
	private Button deleteButton;
	private Tabs tabs;
	private SeTab datasourceTab;
	private SeTab characteristicsTab;
	private SeTab tagMappingTab;
	private SeTab assignedModelsTab;
	private SeTab suggestedModelsTab;
	
	private DataSourceLayout dataSourceLayout;
	
	private FlexLayout content;
	
	public UnitDetailsLayout() {
		addClassName(FLEX_WRAP);
		this.cancelButton = new SeCancelButton();
		this.saveButton = new SeSaveButton();
		this.deleteButton = new SeDeleteButton();
		this.tabs = new Tabs();
		this.tabs.addThemeVariants(TabsVariant.LUMO_EQUAL_WIDTH_TABS);
		this.tabs.addClassNames(WIDTH_FULL, MARGIN_TOP_M);
		datasourceTab = new SeTab(VaadinIcon.DATABASE.create(), new Span());
		characteristicsTab = new SeTab(VaadinIcon.EDIT.create(), new Span());
		tagMappingTab = new SeTab(VaadinIcon.SLIDERS.create(), new Span());
		assignedModelsTab = new SeTab(VaadinIcon.MAP_MARKER.create(), new Span());
		suggestedModelsTab = new SeTab(VaadinIcon.SEARCH_PLUS.create(), new Span());
		this.tabs.add(datasourceTab, characteristicsTab, tagMappingTab, assignedModelsTab, suggestedModelsTab);
		
		FlexLayout buttonLayout = new FlexLayout();
		buttonLayout.setClassName("buttons");
		buttonLayout.add(this.saveButton, this.deleteButton, this.cancelButton);
		this.content = new FlexLayout();
		this.content.addClassNames(MARGIN_TOP_M, ITEM_GAP_HORIZONTAL_M, ITEMS_ALIGN_START, FLEX_WRAP, CONTENT_EVENLY);
		tabs.setSelectedTab(datasourceTab);
		showDatasourceLayout();
		add(this.tabs, this.content);
	}
	
	public void showDatasourceLayout() {
		MultiselectComboBox<String> wts = new MultiselectComboBox<>();
		MultiselectComboBox<String> soe = new MultiselectComboBox<>();
		MultiselectComboBox<String> fr = new MultiselectComboBox<>();
		dataSourceLayout = new DataSourceLayout();
		dataSourceLayout.wts = wts;
		dataSourceLayout.soe = soe;
		dataSourceLayout.fr = fr;
		this.content.add(wts, soe, fr);
	}

	public void clear() {
		this.content.removeAll();
	}
	
	class DataSourceLayout {
		protected MultiselectComboBox<String> wts;
		protected MultiselectComboBox<String> soe;
		protected MultiselectComboBox<String> fr;
	}

	public DataSourceLayout getDataSourceLayout() {
		return dataSourceLayout;
	}

	public Button getCancelButton() {
		return cancelButton;
	}

	public Button getSaveButton() {
		return saveButton;
	}

	public Button getDeleteButton() {
		return deleteButton;
	}

	public Tabs getTabs() {
		return tabs;
	}

	public SeTab getDatasourceTab() {
		return datasourceTab;
	}

	public SeTab getCharacteristicsTab() {
		return characteristicsTab;
	}

	public SeTab getTagMappingTab() {
		return tagMappingTab;
	}

	public SeTab getAssignedModelsTab() {
		return assignedModelsTab;
	}

	public SeTab getSuggestedModelsTab() {
		return suggestedModelsTab;
	}
}