package com.siemens.gs.it.ads;

import java.nio.charset.StandardCharsets;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.server.AppShellSettings;
import com.vaadin.flow.theme.Theme;

/**
 * The entry point of the Spring Boot application.
 */
@SpringBootApplication
@Theme(value = "ads-theme")
public class Application extends SpringBootServletInitializer implements AppShellConfigurator {
	private static final String I18N_MESSAGES = "config/i18n/messages";
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
	
	@Bean
	public MessageSource messageSource() {
	    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
	    messageSource.setBasenames(I18N_MESSAGES);
	    messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
	    return messageSource;
	}

	@Override
	public void configurePage(AppShellSettings settings) {
		settings.addFavIcon("icon", "icons/favicon.ico", "256x256");
	    settings.addLink("shortcut icon", "icons/favicon.ico");
	}
}
