package com.siemens.gs.it.ads.unit.list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.vaadin.tatu.TwinColSelect;

import com.siemens.gs.it.ads.AdsApplicationLayout;
import com.siemens.gs.it.ads.AdsContentLayout;
import com.siemens.gs.it.ads.apptheme.component.SeCancelButton;
import com.siemens.gs.it.ads.apptheme.component.SeOkButton;
import com.siemens.gs.it.ads.apptheme.component.SfIcon;
import com.siemens.gs.it.ads.apptheme.component.SfIcon.SiemensIcon;
import com.siemens.gs.it.ads.core.domain.characteristic.Characteristic;
import com.siemens.gs.it.ads.spring.AdsMessageSource;
import com.siemens.gs.it.ads.spring.security.ApplicationSecurity;
import com.siemens.gs.it.ads.unit.PresentableUnit;
import com.siemens.gs.it.ads.unit.detail.UnitDetailsViewImpl;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.grid.dataview.GridListDataView;
import com.vaadin.flow.component.html.H4;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.page.BrowserWindowResizeEvent;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.data.renderer.Renderer;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import com.vaadin.flow.router.RouteParameters;
import com.vaadin.flow.router.RouterLink;

@Route(value = "unit", layout = AdsApplicationLayout.class)
@RouteAlias(value = "", layout = AdsApplicationLayout.class)
@PreserveOnRefresh
//@AllAuthenticatedUsers
public class UnitViewImpl extends AdsContentLayout implements AfterNavigationObserver, UnitView {
	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(getClass());
	private transient ApplicationEventPublisher eventPublisher;
	private transient AdsMessageSource messageSource;
	private transient ApplicationSecurity applicationSecurity;
	private UnitLayout layout;
	
	private static final String COLUMN_FAVORITE = "favorite";
	private static final String COLUMN_UNIT_NAME = "UNIT_NAME";
	private static final String COLUMN_AEIC = "AEIC";
	private static final String COLUMN_NAME = "unit";
	private static final String COLUMN_PLANT_NAME = "plantName";
	private static final String COLUMN_PLANT_CONFIG = "plantConfig";
	private static final String COLUMN_UNIT_NUMBER = "unitNumber";
	private static final String COLUMN_UNIT_TYPE = "UNIT_TYPE";
	private static final List<String> STEADY_COLUMNS = Arrays.asList(COLUMN_FAVORITE, COLUMN_UNIT_NAME, COLUMN_AEIC);
	private static final List<String> DEFAULT_VISIBLE_COLUMNS = Arrays.asList(COLUMN_NAME, COLUMN_PLANT_NAME, COLUMN_PLANT_CONFIG, COLUMN_UNIT_NUMBER, COLUMN_UNIT_TYPE);
	

	@Autowired
    public UnitViewImpl(AdsMessageSource messageSource, ApplicationEventPublisher eventPublisher, ApplicationSecurity applicationSecurity) {
    	super();
		this.messageSource = messageSource;
		this.eventPublisher = eventPublisher;
		this.applicationSecurity = applicationSecurity;
    }
    
    @PostConstruct
    private void init() {
    	this.layout = new UnitLayout();
		add(this.layout);
    	setHeaderText(this.messageSource.getMessage("view.unit"));
    	Button addRemoveButton = this.layout.getAddRemoveButton();
		addRemoveButton.setText(this.messageSource.getMessage("view.unit.configureColumns"));
		addRemoveButton.addClickListener(click -> {
			this.eventPublisher.publishEvent(new ConfigureColumnsEvent());
		});
    	Button addUnitsButton = this.layout.getAddUnitsButton();
		addUnitsButton.setText(this.messageSource.getMessage("view.unit.addUnits"));
		addUnitsButton.addClickListener(click -> {
			this.eventPublisher.publishEvent(new AddUnitsEvent(Collections.emptyList()));
		});
		Grid<PresentableUnit> unitGrid = this.layout.getUnitGrid();
    	unitGrid.removeAllColumns();
    	unitGrid.addColumn(createFavoriteIconRenderer()).setHeader("").setResizable(false).setWidth("4em").setFlexGrow(0).setKey(COLUMN_FAVORITE);
		Column<PresentableUnit> aeicColumn = unitGrid.addColumn(createAeicRenderer()).setHeader(createTableHeader(this.messageSource.getMessage("entities.unitData.aeic"))).setKey(COLUMN_AEIC);
		Column<PresentableUnit> unitNameColumn = unitGrid.addColumn(createDetailsLinkRenderer()).setHeader(createTableHeader(this.messageSource.getMessage("entities.unitData.name"))).setKey(COLUMN_UNIT_NAME).setSortable(true).setComparator(PresentableUnit::getName);
    	List<GridSortOrder<PresentableUnit>> sorterList = new ArrayList<>();
    	GridSortOrder<PresentableUnit> sortByUnitName = new GridSortOrder<>(unitNameColumn, SortDirection.ASCENDING);
    	sorterList.add(sortByUnitName);
		unitGrid.sort(sorterList);
    }

    @Override
	public void afterNavigation(AfterNavigationEvent event) {
    	this.eventPublisher.publishEvent(this);
    	Grid<PresentableUnit> unitGrid = this.layout.getUnitGrid();
    	Set<String> visibleColumns = unitGrid.getColumns().stream().filter(column -> column.isVisible()).map(Column::getKey).collect(Collectors.toSet());
		this.eventPublisher.publishEvent(new LoadUnitsEvent(visibleColumns, applicationSecurity.getCurrentUser()));
	}
    
    private TextField createFilterHeader(Consumer<String> filterChangeConsumer) {
        TextField textField = new TextField();
        textField.setValueChangeMode(ValueChangeMode.EAGER);
        textField.setClearButtonVisible(true);
        textField.addThemeVariants(TextFieldVariant.LUMO_SMALL);
        textField.setWidthFull();
        textField.getStyle().set("max-width", "20em");
        if (filterChangeConsumer != null) {
        	textField.addValueChangeListener(e -> filterChangeConsumer.accept(e.getValue()));
        }
        return textField;
    }

	private ValueProvider<PresentableUnit, String> createAeicRenderer() {
		return new ValueProvider<PresentableUnit, String>(){
			private static final long serialVersionUID = 1L;
			public String apply(PresentableUnit presentableUnit) {
				      return presentableUnit.getUnit().getUnitMetaData().getAeic();
				   }
				};
	}

	private Span createTableHeader(String text) {
    	Span span = new Span(text);
    	span.setClassName("headercell");
    	return span;
    }
    
    public void populateUnitTable(List<PresentableUnit> items) {
    	Grid<PresentableUnit> unitGrid = this.layout.getUnitGrid();
		GridListDataView<PresentableUnit> dataView = unitGrid.setItems(items);
    	PresentableUnitFilter puFilter = new PresentableUnitFilter(dataView);
    	List<HeaderRow> headerRows = unitGrid.getHeaderRows();
    	
    	boolean createHeader = headerRows.size() == 1;
    	HeaderRow headerRow = createHeader ? unitGrid.appendHeaderRow() : headerRows.get(1);
		headerRow.getCell(unitGrid.getColumnByKey(COLUMN_AEIC)).setComponent(
				createFilterHeader(puFilter::setAeic));
		headerRow.getCell(unitGrid.getColumnByKey(COLUMN_UNIT_NAME)).setComponent(
				createFilterHeader(puFilter::setUnitName));
    	
		for (int columnCounter = 3; columnCounter < unitGrid.getColumns().size(); columnCounter++) {
			Column<PresentableUnit> characteristicColumn = unitGrid.getColumns().get(columnCounter);
			headerRow.getCell(characteristicColumn).setComponent(createFilterHeader(puFilter::setUnitName));
		}
		unitGrid.getUI().ifPresent(ui -> {
			ui.getPage().retrieveExtendedClientDetails(receiver -> {
				this.layout.getUnitGrid().setHeight(Integer.valueOf(receiver.getBodyClientHeight() - 300).floatValue(), Unit.PIXELS);
			});
		});
    }
    
    @EventListener
    private void calculateGridHeight(BrowserWindowResizeEvent event) {
    	this.layout.getUnitGrid().setHeight(Integer.valueOf(event.getHeight() - 300).floatValue(), Unit.PIXELS);
    }
    
    private Renderer<PresentableUnit> createFavoriteIconRenderer() {
    	String tag = String.format("<span area-hidden class=\"%s %s ${item.filled}\" @click=\"${toggleFavorite}\"></span>", SfIcon.ICON_CLASS, SiemensIcon.star.name());
    	return LitRenderer.<PresentableUnit>of(tag)
    			.withProperty("filled", presentableUnit -> {
    		return presentableUnit.isFavorite() ? "filled" : "";
    	}).withFunction("toggleFavorite", presentableUnit -> {
    		presentableUnit.setFavorite(!presentableUnit.isFavorite());
    		if (presentableUnit.isFavorite()) {
    			this.eventPublisher.publishEvent(new AddFavoriteEvent(this.applicationSecurity.getCurrentUser(), presentableUnit.getUnit()));
    		} else {
    			this.eventPublisher.publishEvent(new RemoveFavoriteEvent(this.applicationSecurity.getCurrentUser(), presentableUnit.getUnit()));
    		}
    		this.layout.getUnitGrid().getDataProvider().refreshItem(presentableUnit);
    	});
    }
    
    private Renderer<PresentableUnit> createDetailsLinkRenderer() {
    	ComponentRenderer<RouterLink, PresentableUnit> renderer = new ComponentRenderer<>(presentableUnit -> {
    		RouterLink rl = new RouterLink(presentableUnit.getUnit().getName(), UnitDetailsViewImpl.class, new RouteParameters("unitId", String.valueOf(presentableUnit.getUnit().getId())));
    		rl.setClassName("se-link");
    		return rl;
    	});
    	return renderer;
    }
    
    public void showUnitCharacteristicsDialog(List<Characteristic> characteristics) {
    	List<String> visibleColumnKeys = this.layout.getUnitGrid().getColumns().stream().filter(column -> column.isVisible()).map(column -> column.getKey()).collect(Collectors.toList());
    	visibleColumnKeys.removeIf(key -> STEADY_COLUMNS.contains(key));
    	Set<Characteristic> selectedCharacteristics = characteristics.stream().filter(characteristic -> visibleColumnKeys.contains(String.valueOf(characteristic.getId()))).collect(Collectors.toSet());  
		Dialog dialog = new Dialog();
		dialog.setWidth("1075px");
		dialog.setHeight("700px");
		dialog.setModal(true);
		dialog.setDraggable(true);
		dialog.setResizable(true);
		TwinColSelect<Characteristic> twinColSelect = new TwinColSelect<>();
		twinColSelect.setItems(characteristics);
		twinColSelect.setItemLabelGenerator(item -> item.getName());
		twinColSelect.setHeight(80f, Unit.PERCENTAGE);
		twinColSelect.setValue(selectedCharacteristics);
		Button okButton = new SeOkButton(this.messageSource.getMessage("confirm.ok"));
		okButton.addClickListener(click -> {
			configureColumns(twinColSelect.getValue());
			dialog.close();
		});
		Button cancelButton = new SeCancelButton(this.messageSource.getMessage("action.cancel"), click -> dialog.close());
		FlexLayout buttonLayout = new FlexLayout();
		buttonLayout.getStyle().set("margin-top", "var(--lumo-space-m)");
		buttonLayout.setJustifyContentMode(JustifyContentMode.BETWEEN);
		buttonLayout.add(cancelButton, okButton);
		dialog.add(new H4(this.messageSource.getMessage("view.unit.configureColumns")), twinColSelect, buttonLayout);
		dialog.open();
	}

	private void configureColumns(Set<Characteristic> selectedCharacteristics) {
		Grid<PresentableUnit> unitGrid = this.layout.getUnitGrid();
		unitGrid.getColumns().forEach(column -> {
			if (!STEADY_COLUMNS.contains(column.getKey())) {
				unitGrid.removeColumn(column);
			}
		});
		selectedCharacteristics.forEach(characteristic -> {
			Column<PresentableUnit> column = unitGrid.addColumn(presentableUnit -> presentableUnit.getValuesByCharacteristicId()
					.get(characteristic.getId()))
					.setHeader(createTableHeader(characteristic.getName()))
					.setKey(String.valueOf(characteristic.getId()));
			column.setVisible(true);
			HeaderRow headerRow = unitGrid.getHeaderRows().get(1);
		});
		Set<String> visibleColumns = unitGrid.getColumns().stream().filter(column -> column.isVisible()).map(Column::getKey).collect(Collectors.toSet());
		this.eventPublisher.publishEvent(new LoadUnitsEvent(visibleColumns, applicationSecurity.getCurrentUser()));
	}

	@Override
	public void addUnits() {
		logger.info("addUnits");
	}
}