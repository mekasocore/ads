package com.siemens.gs.it.ads.unit.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;

import com.siemens.gs.it.ads.core.domain.UnitFavorite;
import com.siemens.gs.it.ads.core.domain.characteristic.Characteristic;
import com.siemens.gs.it.ads.core.domain.characteristic.UnitCharacteristicWithValue;
import com.siemens.gs.it.ads.core.domain.unit.UnitData;
import com.siemens.gs.it.ads.core.service.characteristic.CharacteristicService;
import com.siemens.gs.it.ads.core.service.unit.CreateUnitFavoriteUseCase;
import com.siemens.gs.it.ads.core.service.unit.DeleteUnitFavoriteUseCase;
import com.siemens.gs.it.ads.core.service.unit.ReadUnitDataUseCase;
import com.siemens.gs.it.ads.core.service.unit.ReadUnitFavoriteUseCase;
import com.siemens.gs.it.ads.unit.PresentableUnit;
import com.siemens.gs.it.ads.unit.list.UnitView.AddFavoriteEvent;
import com.siemens.gs.it.ads.unit.list.UnitView.AddUnitsEvent;
import com.siemens.gs.it.ads.unit.list.UnitView.ConfigureColumnsEvent;
import com.siemens.gs.it.ads.unit.list.UnitView.LoadUnitsEvent;
import com.siemens.gs.it.ads.unit.list.UnitView.RemoveFavoriteEvent;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.VaadinSessionScope;

@SpringComponent
public class UnitPresenter {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private transient UnitView view;
	private transient ReadUnitDataUseCase readUnitDataUseCase;
	private ReadUnitFavoriteUseCase readUnitFavoriteUseCase;
	private CreateUnitFavoriteUseCase createUnitFavoriteUseCase;
	private DeleteUnitFavoriteUseCase deleteUnitFavoriteUseCase;
	
	private CharacteristicService characteristicService;
	
	@Autowired
	public UnitPresenter(ReadUnitDataUseCase readUnitDataUseCase,
			ReadUnitFavoriteUseCase readUnitFavoriteUseCase,
			CharacteristicService characteristicService,
			CreateUnitFavoriteUseCase createUnitFavoriteUseCase,
			DeleteUnitFavoriteUseCase deleteUnitFavoriteUseCase) {
		this.readUnitDataUseCase = readUnitDataUseCase;
		this.readUnitFavoriteUseCase = readUnitFavoriteUseCase;
		this.characteristicService = characteristicService;
		this.createUnitFavoriteUseCase = createUnitFavoriteUseCase;
		this.deleteUnitFavoriteUseCase = deleteUnitFavoriteUseCase;
	}
	
	@EventListener
	private void init(UnitView view) {
		this.view = view;
	}
	
	@EventListener
	public void loadUnits(LoadUnitsEvent event) {
		List<UnitData> units = this.readUnitDataUseCase.findAll();
		List<UnitFavorite> favorites = this.readUnitFavoriteUseCase.findByUser(event.getAdsUser());
		List<PresentableUnit> presentableUnits = createPresentableUnits(units, favorites, event.getVisibleColumns());
		this.view.populateUnitTable(presentableUnits);
	}
	
	@EventListener
	public void load(ConfigureColumnsEvent event) {
		List<Characteristic> characteristics = this.characteristicService.findAll();
		characteristics.sort((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
		this.view.showUnitCharacteristicsDialog(characteristics);
	}
	
	@EventListener
	public void addFavorite(AddFavoriteEvent event) {
		this.createUnitFavoriteUseCase.create(event.getAdsUser(), event.getUnitData());
	}
	
	@EventListener
	public void removeFavorite(RemoveFavoriteEvent event) {
		this.deleteUnitFavoriteUseCase.delete(event.getAdsUser(), event.getUnitData());
	}
	
	private List<PresentableUnit> createPresentableUnits(List<UnitData> units, List<UnitFavorite> favorites, Set<String> visibleColumns) {
		List<Long> visibleCharacteristicIds = visibleColumns.stream().filter(s -> s.matches("^\\d*$"))
				.map(Long::valueOf).collect(Collectors.toList());
		List<UnitCharacteristicWithValue> unitCharacteristicsWithValues = characteristicService
				.findUnitCharacteristicsWithValue(visibleCharacteristicIds);
		Map<Long, List<UnitCharacteristicWithValue>> unitCharacteristicsByUnit = new HashMap<>();
		for (UnitCharacteristicWithValue uc : unitCharacteristicsWithValues) {
			unitCharacteristicsByUnit.putIfAbsent(uc.getUnitId(), new ArrayList<>());
			unitCharacteristicsByUnit.get(uc.getUnitId()).add(uc);
		}
		Set<UnitData> favoriteUnits = favorites.stream().map(UnitFavorite::getUnit).collect(Collectors.toSet());
		return units.stream()//
				.map(unit -> new PresentableUnit(unit, favoriteUnits.contains(unit),
						unitCharacteristicsByUnit.getOrDefault(unit.getId(), Collections.emptyList())))
				.collect(Collectors.toList());
	}

	@EventListener
	public void addUnits(AddUnitsEvent event) {
		this.view.addUnits();
	}
}