package com.siemens.gs.it.ads.unit.list;

import com.siemens.gs.it.ads.apptheme.component.SeButton;
import com.siemens.gs.it.ads.apptheme.component.SfIcon;
import com.siemens.gs.it.ads.apptheme.component.SfIcon.SiemensIcon;
import com.siemens.gs.it.ads.apptheme.layout.WithStyle;
import com.siemens.gs.it.ads.unit.PresentableUnit;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridVariant;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.FlexLayout;

public class UnitLayout extends FlexLayout implements WithStyle {
	private static final long serialVersionUID = 1L;
	
	private SeButton addRemoveButton;
	private SeButton addUnitsButton;
	private Grid<PresentableUnit> unitGrid;
	
	public UnitLayout() {
		addClassNames(FLEX_WRAP);
		addRemoveButton = new SeButton(SfIcon.withIcon(SiemensIcon.add));
		addUnitsButton = new SeButton(SfIcon.withIcon(SiemensIcon.addCircle));

		unitGrid = new Grid<>(PresentableUnit.class, false);
		unitGrid.setWidth(100.0f, Unit.PERCENTAGE);
		unitGrid.addThemeVariants(GridVariant.LUMO_NO_BORDER, GridVariant.LUMO_NO_ROW_BORDERS);
		add(wrapper(addRemoveButton), wrapper(addUnitsButton), unitGrid);
	}
	
	private Div wrapper(Component component) {
		Div div = new Div(component);
		div.addClassName("item");
		return div;
	}

	public Button getAddRemoveButton() {
		return addRemoveButton.getButton();
	}

	public Button getAddUnitsButton() {
		return addUnitsButton.getButton();
	}

	public Grid<PresentableUnit> getUnitGrid() {
		return unitGrid;
	}
}
