package com.siemens.gs.it.ads.unit.detail;


import com.siemens.gs.it.ads.core.domain.characteristic.UnitCharacteristic;

import java.io.Serializable;

public class PresentableUnitCharacteristic implements Serializable, Comparable<PresentableUnitCharacteristic>
{
   
	private static final long serialVersionUID = 1L;

	private final String name;

    private final String value;

    private final Long id;

    PresentableUnitCharacteristic(UnitCharacteristic unitCharacteristic)
    {
        this.value = unitCharacteristic.getValue();
        this.name = unitCharacteristic.getCharacteristic().getName();
        this.id = unitCharacteristic.getCharacteristic().getId();
    }

    public String getName()
    {
        return name;
    }

    public String getValue()
    {
        return value;
    }

    public Long getId()
    {
        return id;
    }

    public PresentableUnitCharacteristic getSelf() {
        return this;
    }

    @Override
    public int compareTo(PresentableUnitCharacteristic other)
    {
        return this.getName().compareTo(other.getName());
    }
}
