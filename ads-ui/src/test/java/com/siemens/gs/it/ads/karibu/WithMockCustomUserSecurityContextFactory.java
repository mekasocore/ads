package com.siemens.gs.it.ads.karibu;

import java.util.EnumSet;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

public class WithMockCustomUserSecurityContextFactory implements WithSecurityContextFactory<WithAppUser> {

	@Override
	public SecurityContext createSecurityContext(WithAppUser withAppUser) {
		SecurityContext context = SecurityContextHolder.createEmptyContext();
		AppUser appUser = new AppUser();
		appUser.setUsername(withAppUser.userId());
		EnumSet<AppRole> roles = EnumSet.of(AppRole.USER, withAppUser.appRoles());
		Authentication auth = new UsernamePasswordAuthenticationToken(appUser, "password", roles);
		context.setAuthentication(auth);
		return context;
	}

}
