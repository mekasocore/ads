package com.siemens.gs.it.ads.karibu;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.springframework.security.test.context.support.WithSecurityContext;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithMockCustomUserSecurityContextFactory.class)
public @interface WithAppUser {
	AppRole[] appRoles() default {AppRole.USER};

	String userId() default "ADS-User";
}