package com.siemens.gs.it.ads.karibu;

import java.util.Locale;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;

import com.github.mvysny.kaributesting.v10.MockVaadin;
import com.github.mvysny.kaributesting.v10.Routes;
import com.github.mvysny.kaributesting.v10.spring.MockSpringServlet;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.spring.SpringServlet;

import kotlin.jvm.functions.Function0;

@DirtiesContext
@SpringBootTest
public class AbstractKaribuTest {
	private static Routes routes;

	@Autowired
	private ApplicationContext applicationContext;

	@BeforeAll
	public static void discoverRoutes() {
		routes = new Routes().autoDiscoverViews("com.siemens.gs.it.ads");
	}

	@BeforeEach
	public void initUi() {
		Function0<UI> uiFactory = UI::new;
		SpringServlet servlet = new MockSpringServlet(routes, applicationContext, uiFactory);
		MockVaadin.setup(uiFactory, servlet);
		UI.getCurrent().setLocale(Locale.ENGLISH);
	}

	@AfterEach
	public void tearDown() {
		MockVaadin.tearDown();
	}
}
