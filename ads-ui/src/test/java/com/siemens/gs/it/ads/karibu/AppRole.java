package com.siemens.gs.it.ads.karibu;

import org.springframework.security.core.GrantedAuthority;

public enum AppRole implements GrantedAuthority {
	ADMIN, USER
	;

	@Override
	public String getAuthority() {
		return name();
	}
}
