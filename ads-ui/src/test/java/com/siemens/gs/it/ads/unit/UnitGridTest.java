package com.siemens.gs.it.ads.unit;

import static com.github.mvysny.kaributesting.v10.LocatorJ._get;
import static com.github.mvysny.kaributesting.v10.ButtonKt._click;

import java.util.List;
import java.util.function.Predicate;

import org.assertj.core.api.WithAssertions;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.siemens.gs.it.ads.apptheme.component.SeOkButton;
import com.siemens.gs.it.ads.karibu.AbstractKaribuTest;
import com.siemens.gs.it.ads.karibu.AppRole;
import com.siemens.gs.it.ads.karibu.WithAppUser;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;


@WithAppUser(appRoles = {AppRole.ADMIN})
@Disabled
public class UnitGridTest extends AbstractKaribuTest implements WithAssertions {

	@Test
	public void testAdsStart() {
		@NotNull
		H1 headline = _get(H1.class);
		assertThat(headline.getText())
		.isNotEmpty()
		.as("Check the headline")
		.isEqualTo("Units");
	}
	
	@Test
	public void testGrid() {
		@SuppressWarnings("unchecked")
		@NotNull
		Grid<PresentableUnit> grid = _get(Grid.class);
		List<Column<PresentableUnit>> columns = grid.getColumns();
		assertThat(columns)
		.isNotEmpty()
		.as("Grid should have 5 columns")
		.hasSize(5);
		long visibleColumns = columns.stream().filter(column -> column.isVisible()).count();
		assertThat(visibleColumns).isEqualTo(3);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testAddColumnsToGrid() {
		Grid<PresentableUnit> grid = _get(Grid.class);
		List<Column<PresentableUnit>> columns = grid.getColumns();
		long visibleColumns = columns.stream().filter(column -> column.isVisible()).count();
		assertThat(visibleColumns).isEqualTo(3);
		_click(_get(Button.class, spec -> spec.withCaption("Add/Remove Columns")));
		_click(_get(Button.class, spec -> spec.withPredicate(findButtonByIcon(VaadinIcon.ANGLE_DOUBLE_RIGHT))));
		_click(_get(SeOkButton.class));
		visibleColumns = columns.stream().filter(column -> column.isVisible()).count();
		assertThat(visibleColumns).isEqualTo(5);
	}

	private @NotNull Predicate<Button> findButtonByIcon(VaadinIcon icon) {
		return button -> { 
			String targetIconAttribute = icon.create().getElement().getAttribute("icon");
			Component iconComponent = button.getIcon();
			if (iconComponent instanceof Icon) {
				Icon buttonIcon = (Icon)iconComponent;
				String iconAttribute = buttonIcon.getElement().getAttribute("icon");
				return iconAttribute.equals(targetIconAttribute);
			}
			return false;
		};
	}
}
