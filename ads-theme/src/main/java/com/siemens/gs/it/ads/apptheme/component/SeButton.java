package com.siemens.gs.it.ads.apptheme.component;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.html.Div;

public class SeButton extends Composite<Div> {
	private static final long serialVersionUID = 1L;

	private Button button;
	
	public SeButton(Component icon, ComponentEventListener<ClickEvent<Button>> clickListener) {
		this.button = new Button(icon, clickListener);
		init();
	}
	public SeButton(String text, Component icon, ComponentEventListener<ClickEvent<Button>> clickListener) {
		this.button = new Button(text, icon, clickListener);
		init();
	}
	public SeButton(String text, Component icon) {
		this.button = new Button(text, icon);
		init();
	}
	public SeButton(String text, ComponentEventListener<ClickEvent<Button>> clickListener) {
		this.button = new Button(text, clickListener);
		init();
	}
	public SeButton() {
		this.button = new Button();
		init();
	}
	public SeButton(String text) {
		this.button = new Button(text);
		init();
	}
	public SeButton(Component icon) {
		this.button = new Button(icon);
		init();
	}
	
	private void init() {
		this.button.addThemeVariants(ButtonVariant.LUMO_CONTRAST);
		getContent().setClassName("se-button");
		getContent().add(this.button);
	}
	
	public Button getButton() {
		return button;
	}
}