package com.siemens.gs.it.ads.apptheme.layout;

public interface WithStyle {
	/**
	 * Hides content visually while remaining available to screen readers,
	 */
	public static final String SCREEN_READER_ONLY = "sr-only";
	public static final String BACKGROUND_BASE = "bg-base";
	public static final String BACKGROUND_TRANSPARENT = "bg-transparent";
	public static final String BACKGROUND_CONTRAST_5 = "bg-contrast-5";
	public static final String BACKGROUND_CONTRAST_10 = "bg-contrast-10";
	public static final String BACKGROUND_CONTRAST_20 = "bg-contrast-20";
	public static final String BACKGROUND_CONTRAST_30 = "bg-contrast-30";
	public static final String BACKGROUND_CONTRAST_40 = "bg-contrast-40";
	public static final String BACKGROUND_CONTRAST_50 = "bg-contrast-50";
	public static final String BACKGROUND_CONTRAST_60 = "bg-contrast-60";
	public static final String BACKGROUND_CONTRAST_70 = "bg-contrast-70";
	public static final String BACKGROUND_CONTRAST_80 = "bg-contrast-80";
	public static final String BACKGROUND_CONTRAST_90 = "bg-contrast-90";
	public static final String BACKGROUND_CONTRAST = "bg-contrast";
	public static final String BACKGROUND_PRIMARY_10 = "bg-primary-10";
	public static final String BACKGROUND_PRIMARY_50 = "bg-primary-50";
	public static final String BACKGROUND_PRIMARY = "bg-primary";
	public static final String BACKGROUND_ERROR_10 = "bg-error-10";
	public static final String BACKGROUND_ERROR_50 = "bg-error-50";
	public static final String BACKGROUND_ERROR = "bg-error";
	public static final String BACKGROUND_SUCCESS_10 = "bg-success-10";
	public static final String BACKGROUND_SUCCESS_50 = "bg-success-50";
	public static final String BACKGROUND_SUCCESS = "bg-success";
	public static final String NO_BORDER = "border-0";
	public static final String BORDER = "border";
	public static final String BORDER_BOTTOM = "border-b";
	public static final String BORDER_LEFT = "border-l";
	public static final String BORDER_RIGHT = "border-r";
	public static final String BORDER_TOP = "border-t";
	public static final String BORDER_CONTRAST_5 = "border-contrast-5";
	public static final String BORDER_CONTRAST_10 = "border-contrast-10";
	public static final String BORDER_CONTRAST_20 = "border-contrast-20";
	public static final String BORDER_CONTRAST_30 = "border-contrast-30";
	public static final String BORDER_CONTRAST_40 = "border-contrast-40";
	public static final String BORDER_CONTRAST_50 = "border-contrast-50";
	public static final String BORDER_CONTRAST_60 = "border-contrast-60";
	public static final String BORDER_CONTRAST_70 = "border-contrast-70";
	public static final String BORDER_CONTRAST_80 = "border-contrast-80";
	public static final String BORDER_CONTRAST_90 = "border-contrast-90";
	public static final String BORDER_CONTRAST = "border-contrast";
	public static final String BORDER_PRIMARY_10 = "border-primary-10";
	public static final String BORDER_PRIMARY_50 = "border-primary-50";
	public static final String BORDER_PRIMARY = "border-primary";
	public static final String BORDER_ERROR_10 = "border-error-10";
	public static final String BORDER_ERROR_50 = "border-error-50";
	public static final String BORDER_ERROR = "border-error";
	public static final String BORDER_SUCCESS_10 = "border-success-10";
	public static final String BORDER_SUCCESS_50 = "border-success-50";
	public static final String BORDER_SUCCESS = "border-success";
	/**
	 * Classes for setting the border radius of an element.
	 */
	public static final String BORDER_ROUNDED = "rounded-none";
	public static final String BORDER_ROUNDED_S = "rounded-s";
	public static final String BORDER_ROUNDED_M = "rounded-m";
	public static final String BORDER_ROUNDED_L = "rounded-l";
	/**
	 * Classes for applying a box shadow.
	 */
	public static final String BORDER_SHADOW_XS = "shadow-xs";
	public static final String BORDER_SHADOW_S = "shadow-s";
	public static final String BORDER_SHADOW_M = "shadow-m";
	public static final String BORDER_SHADOW_L = "shadow-l";
	public static final String BORDER_SHADOW_XL = "shadow-xl";
	/**
	 * Classes for distributing space around and between items along a flexbox’s cross axis or a grid’s block axis. Applies to flexbox and grid layouts.
	 */
	public static final String CONTENT_CENTER = "content-center";
	public static final String CONTENT_END = "content-end";
	public static final String CONTENT_START = "content-start";
	public static final String CONTENT_AROUND = "content-around";
	public static final String CONTENT_BETWEEN = "content-between";
	public static final String CONTENT_EVENLY = "content-evenly";
	public static final String CONTENT_STRETCH = "content-stretch";
	/**
	 * Classes for aligning items along a flexbox’s cross axis or a grid’s block axis. Applies to flexbox and grid layouts.
	 */
	public static final String ITEMS_ALIGN_BASELINE = "items-baseline";
	public static final String ITEMS_ALIGN_CENTER = "items-center";
	public static final String ITEMS_ALIGN_END = "items-end";
	public static final String ITEMS_ALIGN_START = "items-start";
	public static final String ITEMS_ALIGN_STRETCH = "items-stretch";
	/**
	 * Classes for overriding individual items align-item property. Applies to flexbox and grid items.
	 */
	public static final String ALIGN_SELF_AUTO = "self-auto";
	public static final String ALIGN_SELF_BASELINE = "self-baseline";
	public static final String ALIGN_SELF_CENTER = "self-center";
	public static final String ALIGN_SELF_END = "self-end";
	public static final String ALIGN_SELF_START = "self-start";
	public static final String ALIGN_SELF_STRETCH = "self-stretch";
	/**
	 * Classes for aligning items along a flexbox’s main axis or a grid’s inline axis. Applies to flexbox and grid layouts.
	 */
	public static final String JUSTIFY_CENTER = "justify-center";
	public static final String JUSTIFY_END = "justify-end";
	public static final String JUSTIFY_START = "justify-start";
	public static final String JUSTIFY_AROUND = "justify-around";
	public static final String JUSTIFY_BETWEEN = "justify-between";
	public static final String JUSTIFY_EVENLY = "justify-evenly";
	/**
	 * Classes for setting how items grow and shrink in a flexbox layout. Applies to flexbox items.
	 */
	public static final String FLEX_AUTO = "flex-auto";
	public static final String FLEX_NONE = "flex-none";
	/**
	 * Classes for setting the flex direction of a flexbox layout.
	 */
	public static final String FLEX_DIR_COLUMN = "flex-col";
	public static final String FLEX_DIR_COLUMN_REVERSE = "flex-col-reverse";
	public static final String FLEX_DIR_ROW = "flex-row";
	public static final String FLEX_DIR_ROW_REVERSE = "flex-row-reverse";
	/**
	 * Classes for setting how items grow in a flexbox layout. Applies to flexbox items.
	 */
	public static final String FLEX_GROW_NO = "flex-grow-0";
	public static final String FLEX_GROW = "flex-grow";
	/**
	 * Classes for setting how items shrink in a flexbox layout. Applies to flexbox items.
	 */
	public static final String FLEX_SHRINK_NO = "flex-shrink-0";
	public static final String FLEX_SHRINK = "flex-shrink";
	
	/**
	 * Classes for setting how items wrap in a flexbox layout. Applies to flexbox layouts.
	 */
	public static final String FLEX_NO_WRAP = "flex-nowrap";
	public static final String FLEX_WRAP = "flex-wrap";
	public static final String FLEX_WRAP_REVERSE = "flex-wrap-reverse";
	/**
	 * Classes for defining the space between items in a flexbox or grid layout. Applies to flexbox and grid layouts.
	 */
	public static final String ITEM_GAP_XS = "gap-xs";
	public static final String ITEM_GAP_S = "gap-s";
	public static final String ITEM_GAP_M = "gap-m";
	public static final String ITEM_GAP_L = "gap-l";
	public static final String ITEM_GAP_XL = "gap-xl";
	/**
	 * Classes for defining the horizontal space between items in a flexbox or grid layout. Applies to flexbox and grid layouts.
	 */
	public static final String ITEM_GAP_HORIZONTAL_XS = "gap-x-xs";
	public static final String ITEM_GAP_HORIZONTAL_S = "gap-x-s";
	public static final String ITEM_GAP_HORIZONTAL_M = "gap-x-m";
	public static final String ITEM_GAP_HORIZONTAL_L = "gap-x-l";
	public static final String ITEM_GAP_HORIZONTAL_XL = "gap-x-xl";
	/**
	 * Classes for defining the vertical space between items in a flexbox or grid layout. Applies to flexbox and grid layouts.
	 */
	public static final String ITEM_GAP_VERTICAL_XS = "gap-y-xs";
	public static final String ITEM_GAP_VERTICAL_S = "gap-y-s";
	public static final String ITEM_GAP_VERTICAL_M = "gap-y-m";
	public static final String ITEM_GAP_VERTICAL_L = "gap-y-l";
	public static final String ITEM_GAP_VERTICAL_XL = "gap-y-xl";
	/**
	 * Classes for setting the number of columns in a grid layout.
	 */
	public static final String GRID_COLS_1 = "grid-cols-1";
	public static final String GRID_COLS_2 = "grid-cols-2";
	public static final String GRID_COLS_3 = "grid-cols-3";
	public static final String GRID_COLS_4 = "grid-cols-4";
	public static final String GRID_COLS_5 = "grid-cols-5";
	public static final String GRID_COLS_6 = "grid-cols-6";
	public static final String GRID_COLS_7 = "grid-cols-7";
	public static final String GRID_COLS_8 = "grid-cols-8";
	public static final String GRID_COLS_9 = "grid-cols-9";
	public static final String GRID_COLS_10 = "grid-cols-10";
	public static final String GRID_COLS_11 = "grid-cols-11";
	public static final String GRID_COLS_12 = "grid-cols-12";
	/**
	 * Classes for setting the number of rows in a grid layout.
	 */
	public static final String GRID_ROWS_1 = "grid-rows-1";
	public static final String GRID_ROWS_2 = "grid-rows-2";
	public static final String GRID_ROWS_3 = "grid-rows-3";
	public static final String GRID_ROWS_4 = "grid-rows-4";
	public static final String GRID_ROWS_5 = "grid-rows-5";
	public static final String GRID_ROWS_6 = "grid-rows-6";
	/**
	 * Classes for setting the column span of an item in a grid layout.
	 */
	public static final String GRID_SPAN_1 = "col-span-1";
	public static final String GRID_SPAN_2 = "col-span-2";
	public static final String GRID_SPAN_3 = "col-span-3";
	public static final String GRID_SPAN_4 = "col-span-4";
	public static final String GRID_SPAN_5 = "col-span-5";
	public static final String GRID_SPAN_6 = "col-span-6";
	public static final String GRID_SPAN_7 = "col-span-7";
	public static final String GRID_SPAN_8 = "col-span-8";
	public static final String GRID_SPAN_9 = "col-span-9";
	public static final String GRID_SPAN_10 = "col-span-10";
	public static final String GRID_SPAN_11 = "col-span-11";
	public static final String GRID_SPAN_12 = "col-span-12";
	/**
	 * Classes for setting the row span of an item in a grid layout.
	 */
	public static final String GRID_ROW_SPAN_1 = "row-span-1";
	public static final String GRID_ROW_SPAN_2 = "row-span-2";
	public static final String GRID_ROW_SPAN_3 = "row-span-3";
	public static final String GRID_ROW_SPAN_4 = "row-span-4";
	public static final String GRID_ROW_SPAN_5 = "row-span-5";
	public static final String GRID_ROW_SPAN_6 = "row-span-6";
	/**
	 * Classes for setting the box sizing property of an element. Box sizing determines whether an element’s border and padding is considered a part of its size.
	 */
	public static final String BOX_BORDER = "box-border";
	public static final String BOX_CONTENT = "box-content";
	/**
	 * Classes for setting the display property of an element. Determines whether the element is a block or inline element and how its items are laid out.
	 */
	public static final String BLOCK = "block";
	public static final String INLINE = "inline";
	public static final String INLINE_BLOCK = "inline-block";
	public static final String FLEX = "flex";
	public static final String INLINE_FLEX = "inline-flex";
	public static final String HIDDEN = "hidden";
	public static final String GRID = "grid";
	public static final String INLINE_GRID = "inline-grid";
	/**
	 * Classes for setting the overflow behaviour of an element.
	 */
	public static final String OVERFLOW_AUTO = "overflow-auto";
	public static final String OVERFLOW_HIDDEN = "overflow-hidden";
	public static final String OVERFLOW_SCROLL = "overflow-scroll";
	/**
	 * Classes for setting the position of an element.
	 */
	public static final String POS_ABSOLUTE = "absolute";
	public static final String POS_FIXED = "fixed";
	public static final String POS_STATIC = "static";
	public static final String POS_STICKY = "sticky";
	public static final String POS_RELATIVE = "relative";
	/**
	 * Classes for setting height and width of an element.
	 */
	public static final String HEIGHT_0 = "h-0";
	public static final String HEIGHT_XS = "h-xs";
	public static final String HEIGHT_S = "h-s";
	public static final String HEIGHT_M = "h-m";
	public static final String HEIGHT_L = "h-l";
	public static final String HEIGHT_XL = "h-xl";
	public static final String HEIGHT_AUTO = "h-auto";
	public static final String HEIGHT_FULL = "h-full";
	public static final String HEIGHT_SCREEN = "h-screen";
	/**
	 * Sets the maximum height of an element to 100% of its parent’s height.
	 */
	public static final String HEIGHT_MAX_FULL = "max-h-full";
	/**
	 * Sets the maximum height of an element to 100% of the viewport height.
	 */
	public static final String HEIGHT_MAX_SCREEN = "max-h-screen";
	/**
	 * Sets the minimum height of an element to 0.
	 */
	public static final String HEIGHT_MIN_0 = "min-h-0";
	/**
	 * Sets the minimum height of an element to 100% of its parent’s height.
	 */
	public static final String HEIGHT_MIN_FULL = "min-h-full";
	/**
	 * Sets the minimum height of an element to 100% of the viewport height.
	 */
	public static final String HEIGHT_MIN_SCREEN = "min-h-screen";
	public static final String WIDTH_XS = "w-xs";
	public static final String WIDTH_S = "w-s";
	public static final String WIDTH_M = "w-m";
	public static final String WIDTH_L = "w-l";
	public static final String WIDTH_XL = "w-xl";
	public static final String WIDTH_AUTO = "w-auto";
	/**
	 * Sets the width of an element to 100% of its parent’s width.
	 */
	public static final String WIDTH_FULL = "w-full";
	/**
	 * Sets the maximum width of an element to 100% of its parent’s width
	 */
	public static final String WIDT_MAX_FULL = "max-w-full";
	/**
	 * Sets the maximum width of an element to 640px.
	 */
	public static final String WIDT_MAX_SM = "max-w-screen-sm";
	/**
	 * Sets the maximum width of an element to 768px.
	 */
	public static final String WIDT_MAX_MD = "max-w-screen-md";
	/**
	 * Sets the maximum width of an element to 1024px.
	 */
	public static final String WIDT_MAX_LG = "max-w-screen-lg";
	/**
	 * Sets the maximum width of an element to 1280px.
	 */
	public static final String WIDT_MAX_XL = "max-w-screen-xl";
	/**
	 * Sets the maximum width of an element to 1536px.
	 */
	public static final String WIDT_MAX_XXL = "max-w-screen-2xl";
	/**
	 * Sets the minimum width of an element to 0.
	 */
	public static final String WIDTH_MIN_0 = "min-w-0";
	/**
	 * Sets the minimum width of an element to 100% of its parent’s width.
	 */
	public static final String WIDTH_MIN_FULL = "min-w-full";
	public static final String ICON_S = "icon-s";
	public static final String ICON_M = "icon-m";
	public static final String ICON_L = "icon-l";
	public static final String MARGIN_AUTO = "m-auto";
	public static final String MARGIN_NO = "m-0";
	public static final String MARGIN_XS = "m-xs";
	public static final String MARGIN_S = "m-s";
	public static final String MARGIN_M = "m-m";
	public static final String MARGIN_L = "m-l";
	public static final String MARGIN_XL = "m-xl";
	public static final String MARGIN_BOTTOM_AUTO = "mb-auto";
	public static final String MARGIN_BOTTOM_NO = "mb-0";
	public static final String MARGIN_BOTTOM_XS = "mb-xs";
	public static final String MARGIN_BOTTOM_S = "mb-s";
	public static final String MARGIN_BOTTOM_M = "mb-m";
	public static final String MARGIN_BOTTOM_L = "mb-l";
	public static final String MARGIN_BOTTOM_XL = "mb-xl";
	public static final String MARGIN_LEFT_AUTO = "ml-auto";
	public static final String MARGIN_LEFT_NO = "ml-0";
	public static final String MARGIN_LEFT_XS = "ml-xs";
	public static final String MARGIN_LEFT_S = "ml-s";
	public static final String MARGIN_LEFT_M = "ml-m";
	public static final String MARGIN_LEFT_L = "ml-l";
	public static final String MARGIN_LEFT_XL = "ml-xl";
	public static final String MARGIN_RIGHT_AUTO = "mr-auto";
	public static final String MARGIN_RIGHT_NO = "mr-0";
	public static final String MARGIN_RIGHT_XS = "mr-xs";
	public static final String MARGIN_RIGHT_S = "mr-s";
	public static final String MARGIN_RIGHT_M = "mr-m";
	public static final String MARGIN_RIGHT_L = "mr-l";
	public static final String MARGIN_RIGHT_XL = "mr-xl";
	public static final String MARGIN_TOP_AUTO = "mt-auto";
	public static final String MARGIN_TOP_NO = "mt-0";
	public static final String MARGIN_TOP_XS = "mt-xs";
	public static final String MARGIN_TOP_S = "mt-s";
	public static final String MARGIN_TOP_M = "mt-m";
	public static final String MARGIN_TOP_L = "mt-l";
	public static final String MARGIN_TOP_XL = "mt-xl";
	public static final String MARGIN_INLINE_END_AUTO = "me-auto";
	public static final String MARGIN_INLINE_END_NO = "me-0";
	public static final String MARGIN_INLINE_END_XS = "me-xs";
	public static final String MARGIN_INLINE_END_S = "me-s";
	public static final String MARGIN_INLINE_END_M = "me-m";
	public static final String MARGIN_INLINE_END_L = "me-l";
	public static final String MARGIN_INLINE_END_XL = "me-xl";
	public static final String MARGIN_INLINE_START_AUTO = "ms-auto";
	public static final String MARGIN_INLINE_START_NO = "ms-0";
	public static final String MARGIN_INLINE_START_XS = "ms-xs";
	public static final String MARGIN_INLINE_START_S = "ms-s";
	public static final String MARGIN_INLINE_START_M = "ms-m";
	public static final String MARGIN_INLINE_START_L = "ms-l";
	public static final String MARGIN_INLINE_START_XL = "ms-xl";
	public static final String MARGIN_INLINE_HORIZONTAL_AUTO = "mx-auto";
	public static final String MARGIN_INLINE_HORIZONTAL_NO = "mx-0";
	public static final String MARGIN_INLINE_HORIZONTAL_XS = "mx-xs";
	public static final String MARGIN_INLINE_HORIZONTAL_S = "mx-s";
	public static final String MARGIN_INLINE_HORIZONTAL_M = "mx-m";
	public static final String MARGIN_INLINE_HORIZONTAL_L = "mx-l";
	public static final String MARGIN_INLINE_HORIZONTAL_XL = "mx-xl";
	public static final String MARGIN_INLINE_VERTICAL_AUTO = "my-auto";
	public static final String MARGIN_INLINE_VERTICAL_NO = "my-0";
	public static final String MARGIN_INLINE_VERTICAL_XS = "my-xs";
	public static final String MARGIN_INLINE_VERTICAL_S = "my-s";
	public static final String MARGIN_INLINE_VERTICAL_M = "my-m";
	public static final String MARGIN_INLINE_VERTICAL_L = "my-l";
	public static final String MARGIN_INLINE_VERTICAL_XL = "my-xl";
	public static final String PADDING_NO = "p-0";
	public static final String PADDING_XS = "p-xs";
	public static final String PADDING_S = "p-s";
	public static final String PADDING_M = "p-m";
	public static final String PADDING_L = "p-l";
	public static final String PADDING_XL = "p-xl";
	public static final String PADDING_BOTTOM_NO = "pb-0";
	public static final String PADDING_BOTTOM_XS = "pb-xs";
	public static final String PADDING_BOTTOM_S = "pb-s";
	public static final String PADDING_BOTTOM_M = "pb-m";
	public static final String PADDING_BOTTOM_L = "pb-l";
	public static final String PADDING_BOTTOM_XL = "pb-xl";
	public static final String PADDING_LEFT_NO = "pl-0";
	public static final String PADDING_LEFT_XS = "pl-xs";
	public static final String PADDING_LEFT_S = "pl-s";
	public static final String PADDING_LEFT_M = "pl-m";
	public static final String PADDING_LEFT_L = "pl-l";
	public static final String PADDING_LEFT_XL = "pl-xl";
	public static final String PADDING_RIGHT_NO = "pr-0";
	public static final String PADDING_RIGHT_XS = "pr-xs";
	public static final String PADDING_RIGHT_S = "pr-s";
	public static final String PADDING_RIGHT_M = "pr-m";
	public static final String PADDING_RIGHT_L = "pr-l";
	public static final String PADDING_RIGHT_XL = "pr-xl";
	public static final String PADDING_TOP_NO = "pt-0";
	public static final String PADDING_TOP_XS = "pt-xs";
	public static final String PADDING_TOP_S = "pt-s";
	public static final String PADDING_TOP_M = "pt-m";
	public static final String PADDING_TOP_L = "pt-l";
	public static final String PADDING_TOP_XL = "pt-xl";
	public static final String PADDING_INLINE_END_NO = "pe-0";
	public static final String PADDING_INLINE_END_XS = "pe-xs";
	public static final String PADDING_INLINE_END_S = "pe-s";
	public static final String PADDING_INLINE_END_M = "pe-m";
	public static final String PADDING_INLINE_END_L = "pe-l";
	public static final String PADDING_INLINE_END_XL = "pe-xl";
	public static final String PADDING_INLINE_START_NO = "ps-0";
	public static final String PADDING_INLINE_START_XS = "ps-xs";
	public static final String PADDING_INLINE_START_S = "ps-s";
	public static final String PADDING_INLINE_START_M = "ps-m";
	public static final String PADDING_INLINE_START_L = "ps-l";
	public static final String PADDING_INLINE_START_XL = "ps-xl";
	public static final String PADDING_INLINE_HORIZONTAL_NO = "px-0";
	public static final String PADDING_INLINE_HORIZONTAL_XS = "px-xs";
	public static final String PADDING_INLINE_HORIZONTAL_S = "px-s";
	public static final String PADDING_INLINE_HORIZONTAL_M = "px-m";
	public static final String PADDING_INLINE_HORIZONTAL_L = "px-l";
	public static final String PADDING_INLINE_HORIZONTAL_XL = "px-xl";
	public static final String PADDING_INLINE_VERTICAL_NO = "py-0";
	public static final String PADDING_INLINE_VERTICAL_XS = "py-xs";
	public static final String PADDING_INLINE_VERTICAL_S = "py-s";
	public static final String PADDING_INLINE_VERTICAL_M = "py-m";
	public static final String PADDING_INLINE_VERTICAL_L = "py-l";
	public static final String PADDING_INLINE_VERTICAL_XL = "py-xl";
	public static final String TEXT_SIZE_XXS = "text-2xs";
	public static final String TEXT_SIZE_XS = "text-xs";
	public static final String TEXT_SIZE_S = "text-s";
	public static final String TEXT_SIZE_M = "text-m";
	public static final String TEXT_SIZE_L = "text-l";
	public static final String TEXT_SIZE_XL = "text-xl";
	public static final String TEXT_SIZE_XXL = "text-2xl";
	public static final String TEXT_SIZE_XXXL = "text-3xl";
	public static final String FONT_THIN = "font-thin";
	public static final String FONT_XTR_LIGHT = "font-extralight";
	public static final String FONT_LIGHT = "font-light";
	public static final String FONT_NORMAL = "font-normal";
	public static final String FONT_MEDIUM = "font-medium";
	public static final String FONT_SEMIBOLD = "font-semibold";
	public static final String FONT_BOLD = "font-bold";
	public static final String FONT_XTR_BOLD = "font-extrabold";
	public static final String FONT_BLACK = "font-black";
	/**
	 * Classes for setting the line height of an element.
	 */
	public static final String LINE_HEIGHT_NO = "leading-none";
	public static final String LINE_HEIGHT_XS = "leading-xs";
	public static final String LINE_HEIGHT_S = "leading-s";
	public static final String LINE_HEIGHT_M = "leading-m";
	public static final String LIST_NONE = "list-none";
	public static final String TEXT_ALIGN_LEFT = "text-left";
	public static final String TEXT_ALIGN_CENTER = "text-center";
	public static final String TEXT_ALIGN_RIGHT = "text-right";
	public static final String TEXT_ALIGN_JUSTIFY = "text-justify";
	public static final String TEXT_COLOR_HEADER = "text-header";
	public static final String TEXT_COLOR_BODY = "text-body";
	public static final String TEXT_COLOR_SECONDARY = "text-secondary";
	public static final String TEXT_COLOR_TERTIARY = "text-tertiary";
	public static final String TEXT_COLOR_DISABLED = "text-disabled";
	public static final String TEXT_COLOR_PRIMARY = "text-primary";
	public static final String TEXT_COLOR_PRIMARY_CONTRAST = "text-primary-contrast";
	public static final String TEXT_COLOR_ERROR = "text-error";
	public static final String TEXT_COLOR_ERROR_CONTRAST = "text-error-contrast";
	public static final String TEXT_COLOR_SUCCESS = "text-success";
	public static final String TEXT_COLOR_SUCCESS_CONTRAST = "text-success-contrast";
	public static final String TEXT_OVERFLOW_CLIP = "overflow-clip";
	public static final String TEXT_OVERFLOW_ELLIPSIS = "overflow-ellipsis";
	public static final String TEXT_TRANSFORM_CAPITALIZE = "capitalize";
	public static final String TEXT_TRANSFORM_LOWERCASE = "lowercase";
	public static final String TEXT_TRANSFORM_UPPERCASE = "uppercase";
	public static final String TEXT_WHITESPACE_NORMAL = "whitespace-normal";
	public static final String TEXT_WHITESPACE_NOWRAP = "whitespace-nowrap";
	public static final String TEXT_WHITESPACE_PRE = "whitespace-pre";
	public static final String TEXT_WHITESPACE_PRE_LINE = "whitespace-pre-line";
	public static final String TEXT_WHITESPACE_PRE_WRAP = "whitespace-pre-wrap";
}
