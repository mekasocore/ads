package com.siemens.gs.it.ads.apptheme.component;

import com.siemens.gs.it.ads.apptheme.layout.WithStyle;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.accordion.AccordionPanel;
import com.vaadin.flow.component.details.DetailsVariant;

public class SeAccordion extends Accordion implements WithStyle {
	private static final long serialVersionUID = 1L;

	@Override
	public AccordionPanel add(AccordionPanel panel) {
		panel.addThemeVariants(DetailsVariant.REVERSE);
		return super.add(panel);
	}
	
	@Override
	public AccordionPanel add(String summary, Component content) {
		AccordionPanel ap = super.add(summary, content);
		return ap;
	}
}
