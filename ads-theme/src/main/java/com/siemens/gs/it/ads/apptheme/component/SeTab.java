package com.siemens.gs.it.ads.apptheme.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.tabs.Tab;

public class SeTab extends Tab {
	private static final long serialVersionUID = 1L;

	private Icon icon;
	private Span labelSpan;

	public SeTab(Component... components) {
		super(components);
		if (components[0] instanceof Icon) {
			this.icon = (Icon) components[0];
		}
		if (components.length > 0 && components[1] instanceof Span) {
			this.labelSpan = (Span) components[1]; 
		}
	}

	public Icon getIcon() {
		return icon;
	}

	public void setIcon(Icon icon) {
		this.icon = icon;
	}

	public Span getLabelSpan() {
		return labelSpan;
	}

	public void setLabelSpan(Span label) {
		this.labelSpan = label;
	}
}
