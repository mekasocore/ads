package com.siemens.gs.it.ads.apptheme.layout;

import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.router.HasDynamicTitle;

public class SeContentLayout extends FlexLayout implements HasDynamicTitle, WithStyle {

	private H1 header;
	private static final long serialVersionUID = 1L;
	
	public SeContentLayout() {
		this.header = new H1();
		this.header.addClassNames(MARGIN_NO, MARGIN_BOTTOM_M);
		addClassNames(FLEX_DIR_COLUMN, MARGIN_M);
		add(this.header);
	}
	
	public void setHeaderText(String headerText) {
		this.header.setText(headerText);
	}

	@Override
	public String getPageTitle() {
		return this.header.getText();
	}
}
