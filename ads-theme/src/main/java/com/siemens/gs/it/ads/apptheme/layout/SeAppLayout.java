package com.siemens.gs.it.ads.apptheme.layout;

import java.util.List;
import java.util.Optional;

import com.siemens.gs.it.ads.apptheme.component.SfIcon;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Footer;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.Header;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.Nav;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.html.UnorderedList;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.FlexLayout.FlexDirection;
import com.vaadin.flow.router.HighlightCondition;
import com.vaadin.flow.router.RouterLink;

public abstract class SeAppLayout extends AppLayout implements WithStyle {
	private static final long serialVersionUID = 1L;
	
	// the header navigation elements
	private FlexLayout navbarLayout;
	private FlexLayout primaryNavigation;

	// the drawer elements
	private FlexLayout drawerContentLayout;
	private Header header;
	private Nav secondaryNavigation;
	private Footer footer;
	
	public SeAppLayout() {
		setPrimarySection(Section.NAVBAR);
		// top navigation bar
        this.navbarLayout = new FlexLayout();
        this.navbarLayout.setClassName("navbar");
        this.navbarLayout.setFlexDirection(FlexDirection.ROW);
        DrawerToggle toggle = new DrawerToggle();
        toggle.getElement().setAttribute("aria-label", "Menu toggle");
        Div logo = new Div();
        logo.setClassName("se-logo");
        this.primaryNavigation = new FlexLayout();
        this.primaryNavigation.addClassName(FLEX_GROW);
        this.navbarLayout.add(toggle, logo, this.primaryNavigation);
        addToNavbar(true, this.navbarLayout);
        
        // drawer
		this.drawerContentLayout = new FlexLayout();
		this.drawerContentLayout.addClassNames(FLEX, FLEX_DIR_COLUMN, ITEMS_ALIGN_STRETCH, HEIGHT_MAX_FULL, HEIGHT_MIN_FULL);
		this.header = new Header();
		this.header.addClassNames(MARGIN_TOP_M);
		this.secondaryNavigation = new Nav();
		this.secondaryNavigation.addClassNames(FLEX_GROW, OVERFLOW_AUTO);
		this.secondaryNavigation.getElement().setAttribute("aria-labelledby", "views");
		this.footer = createFooter();
        this.drawerContentLayout.add(this.header, this.secondaryNavigation, this.footer);
        addToDrawer(drawerContentLayout);
	}
	
	public void showApplicationInformation(ApplicationInformation applicationInformation) {
		H2 headline = new H2(applicationInformation.name);
		headline.addClassNames(MARGIN_NO, MARGIN_LEFT_M);
    	this.header.add(headline);
    	applicationInformation.getFullName().ifPresent(fullName -> {
    		H5 subtitle = new H5(fullName);
    		subtitle.addClassNames(MARGIN_NO, MARGIN_LEFT_M);
    		this.header.add(subtitle);
    	});
    	applicationInformation.getVersion().ifPresent(version -> {
        	Span name = new Span(version);
        	name.addClassNames(TEXT_SIZE_XXS, TEXT_COLOR_SECONDARY);
        	this.footer.add(name);
        });
	}
	
	public void showSecondaryNavigation(List<MenuItemInfo> menuItems) {
		UnorderedList list = new UnorderedList();
        list.addClassNames(LIST_NONE, MARGIN_NO, PADDING_NO);
        for (MenuItemInfo menuItemInfo : menuItems) {
            ListItem item = new ListItem(createLink(menuItemInfo));
            list.add(item);
        }
        this.secondaryNavigation.add(list);

	}
	
	
	private Footer createFooter() {
        Footer layout = new Footer();
        layout.addClassNames(FLEX, ITEMS_ALIGN_CENTER, MARGIN_INLINE_VERTICAL_S, PADDING_INLINE_HORIZONTAL_M, PADDING_INLINE_VERTICAL_XS);
        return layout;
    }
	
	public void showAdditionalContent(Component [] components) {
		this.navbarLayout.add(components);
	}
	
	private RouterLink createLink(MenuItemInfo menuItemInfo) {
    	RouterLink link = new RouterLink();
        link.addClassNames(FLEX, MARGIN_INLINE_HORIZONTAL_S, PADDING_S, POS_RELATIVE);
        link.setRoute(menuItemInfo.getView());
        if (menuItemInfo.isDefaultView()) {
        	link.setHighlightCondition(this.buildHighLightConditionFor(menuItemInfo.getView(), menuItemInfo.getSubViews()));
        }
        Span text = new Span(menuItemInfo.getText());
        text.addClassNames(FONT_SEMIBOLD, TEXT_SIZE_S);
        link.add(text);
        return link;
    }
	
	private HighlightCondition<RouterLink> buildHighLightConditionFor(Class<?> linkTargetClass, List<Class<? extends Component>> subViews) {
        return (link, afterNavigationEvent) -> {
            return afterNavigationEvent.getActiveChain()
                    .stream()
                    .anyMatch(element -> element.getClass() == linkTargetClass || subViews != null && subViews.contains(element.getClass()));
        };
    }

	public static class MenuItemInfo {
        private String text;
        private SfIcon icon;
        private boolean defaultView;
        private Class<? extends Component> view;
        private List<Class<? extends Component>> subViews;

        public static MenuItemInfo create() {
        	return new MenuItemInfo();
        }
        
        public MenuItemInfo withText(String text) {
        	this.text = text;
        	return this;
        }
        
        public MenuItemInfo withView(Class<? extends Component> view) {
        	this.view = view;
        	return this;
        }
        
        public MenuItemInfo withIcon(SfIcon icon) {
        	this.icon = icon;
        	return this;
        }
        
        public MenuItemInfo withDefaultView(boolean defaultView) {
        	this.defaultView = defaultView;
        	return this;
        }
        
        public MenuItemInfo withSubViews(List<Class<? extends Component>> subViews) {
        	this.subViews = subViews;
        	return this;
        }

        public List<Class<? extends Component>> getSubViews() {
			return subViews;
		}

		public String getText() {
            return text;
        }

        public Class<? extends Component> getView() {
            return view;
        }

		public SfIcon getIcon() {
			return icon;
		}

		public boolean isDefaultView() {
			return defaultView;
		}
    }
	
	public static class ApplicationInformation {
		private String name;
		private String fullName;
		private String version;
		
		public ApplicationInformation(String name, String fullName, String version) {
			this.name = name;
			this.fullName = fullName;
			this.version = version;
		}

		public String getName() {
			return name;
		}

		public Optional<String> getFullName() {
			return Optional.ofNullable(fullName);
		}

		public Optional<String> getVersion() {
			return Optional.ofNullable(version);
		}
	}
}
