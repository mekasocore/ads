package com.siemens.gs.it.ads.apptheme.component;

import com.siemens.gs.it.ads.apptheme.component.SfIcon.SiemensIcon;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;

public class SeCancelButton extends Button {
	private static final long serialVersionUID = 1L;
	public SeCancelButton() {
		setIcon(SfIcon.withIcon(SiemensIcon.cancel));
	}
	public SeCancelButton(String text, ComponentEventListener<ClickEvent<Button>> clickListener) {
		super(text, clickListener);
		setIcon(SfIcon.withIcon(SiemensIcon.cancel));
	}
	public SeCancelButton(String text) {
		super(text);
		setIcon(SfIcon.withIcon(SiemensIcon.cancel));
	}	
}