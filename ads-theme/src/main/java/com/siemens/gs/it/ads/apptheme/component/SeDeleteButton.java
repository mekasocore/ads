package com.siemens.gs.it.ads.apptheme.component;

import com.siemens.gs.it.ads.apptheme.component.SfIcon.SiemensIcon;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;

public class SeDeleteButton extends Button {

	private static final long serialVersionUID = 1L;
	public SeDeleteButton() {
		super();
		setIcon(SfIcon.withIcon(SiemensIcon.delete));
		addThemeVariants(ButtonVariant.LUMO_PRIMARY);
	}
	public SeDeleteButton(String text, ComponentEventListener<ClickEvent<Button>> clickListener) {
		super(text, clickListener);
		setIcon(SfIcon.withIcon(SiemensIcon.delete));
		addThemeVariants(ButtonVariant.LUMO_PRIMARY);
	}
	public SeDeleteButton(String text) {
		super(text);
		setIcon(SfIcon.withIcon(SiemensIcon.delete));
		addThemeVariants(ButtonVariant.LUMO_PRIMARY);
	}
}