package com.siemens.gs.it.ads.apptheme.component;

import java.util.EnumSet;

import com.vaadin.flow.component.html.Span;

/**
 * Siemens Font Icon.
 */
public class SfIcon extends Span {

	private static final long serialVersionUID = 1L;
	public static final String ICON_CLASS = "iconMdsp";
	private static final String BOLD_CLASS = "bold";
	private static final String FILLED_CLASS = "filled";
	public enum IconSize  {
		SMALL, MEDIUM, BIG
	}
	private static final EnumSet<SiemensIcon> BAD_NAMES = EnumSet.of(SiemensIcon.Package, SiemensIcon.Import);

	public enum SiemensIcon {
		add, addCircle, analysis, announcement, apps, archive, arrowDownExtraSmall,
		arrowLeftExtraSmall, arrowRightExtraSmall, arrowSingleDown, arrowSingleLeft,
		arrowSingleRight, arrowSingleUp, arrowUpExtraSmall, aspects, asset,
		assetNetwork, attachment, attention, backward, buildingBlock, calendar,
		cancel, cancelCircle, certificate, chartBarHorizontal, chartBarStacked,
		chartBarVertical, chartDonut, chartDurationCurve, chartGantt, chartGauge,
		chartHeatmap, chartLine, chartMap, chartPie, chartPolar, chartRadar,
		chartTrend, chartSankey, chartValueHorizontal, chartValueVertical,
		check, clipboard, gearBox, comment, coin, coinStack, control, createWorkbook,
		dashboard, dashforward, delete, dragDrop, dropzone, graph, draw, duplicate,
		edit, editDocument, electricity, element, email, errorAlert, exclamationDiamond,
		exit, export, factory, factoryMagnifier, fileTree, filetypeDefault, filter,
		filterAlternative, filterList, flagSmall, flow, folder, forward, frames, fullscreen,
		fullscreenExit, globalSettings, hatMan, headset, hierarchy, home, iconMissing,
		image, Import, information, legal, length, listBullet, locationMarker, lockClosed,
		lockOpened, maintenance, measure, menu, more, moveToTop, next, note, notification,
		notificationSquare, ok, outgoing, outgoingSmall, Package, pause, person, personMultiple,
		phone, play, pressure, previous, question, record, redo, remove, removeCircle, roles,
		rules, save, scope, scopeAlternative, search, selectAll, selectNone, serviceCredential,
		settings, share, shareAlternative, shift, shoppingCart, sidebar, sort, star, starHalf,
		stop, subtenant, tag, temperature, ticket, tile, types, update, undo, updateGear,
		updateNotification, upload, visibilityHidden, visibilityShown, voltage, widget
	}
	
	private SfIcon(SiemensIcon icon) {
		getElement().setAttribute("aria-hidden", true);
		addClassName(ICON_CLASS);
		if (BAD_NAMES.contains(icon)) {
			addClassName(icon.name().toLowerCase());
		} else {
			addClassName(icon.name());
		}
	}
	
	public static SfIcon withIcon(SiemensIcon icon) {
		return new SfIcon(icon);
	}
	
	public SfIcon bold() {
		addClassName(BOLD_CLASS);
		return this;
	}
	
	public SfIcon filled() {
		addClassName(FILLED_CLASS);
		return this;
	}
	
	public SfIcon withSize(IconSize size) {
		if (size != IconSize.MEDIUM) {
			addClassName("icon-" + size.name().toLowerCase());
		}
		return this;
	}
}
